<?php

include_once('vendor/php-jwt/BeforeValidException.php');
include_once('vendor/php-jwt/ExpiredException.php');
include_once('vendor/php-jwt/JWT.php');
include_once('vendor/php-jwt/SignatureInvalidException.php');
include_once('model/security.php');
include_once('bd.php');
/**
 * Implementación del JWT en un objeto
 */

use Firebase\JWT\JWT;

class jwtoken {
    /**
    * Genera el token para ser enviado, se llama unicamente desde el login del usuario
    **/

    private $time = 0;
    private $encrypt = ['HS256'];

    public static function SetJWT($usr_id, $usr_user, $usr_profile, $key) {
        $time = time();
        $token = array(
            "iat" => $time,
            "exp" => $time + (60*60),
            "aud" => self::Aud(),
            "data" => [
                "id" => $usr_id,
                "user" => $usr_user,
                "profile" => $usr_profile
            ]
        );
        return JWT::encode($token, $key[0]['valor']);
    }

	public static function SetJWTMail($usr_id, $usr_user, $key) {
        $time = time();
        $token = array(
            "iat" => $time,
            "exp" => $time + (60*60),
            "aud" => self::Aud(),
            "data" => [
                "id" => $usr_id,
                "user" => $usr_user
            ]
        );
        return JWT::encode($token, $key[0]['valor']);
    }

    public function GetJWT($token, $key) {
        if(empty($token)) {
            throw new Exception("Invalid token supplied.");
        }

        $encrypt = $this->encrypt;
        try {
            $decode = JWT::decode(
                $token,
                $key[0]['valor'],
                $encrypt
            );
        } catch(\Firebase\JWT\ExpiredException $e) {
            $decode = array('Caught exception: ',  $e->getMessage(), "\n");
        } catch(\Firebase\JWT\SignatureInvalidException $e) {
            $decode = array('Caught exception: ',  $e->getMessage(), "\n");
        }

        if (gettype($decode) == 'object'){
            if($decode->aud !== self::Aud()) {
                throw new Exception("Token invalido.");
            }
        }

        return $decode;
    }

    private static function Aud() {
        $aud = '';
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $aud = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $aud = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $aud = $_SERVER['REMOTE_ADDR'];
        }
        $aud .= $_SERVER['HTTP_USER_AGENT'];
        $aud .= gethostname();
        return sha1($aud);
    }
}
?>
