<?php
// Archivo de configuracion
include_once('config/config.php');

class conn {
    // Función de conexión
    function Conn() {
        $conn = null;
        try{
            $conn = new PDO(dns . ":host=" . host  . ";dbname=" . dbname, user, pass);
            $conn->exec("set names utf8");
        } catch(PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }
        return $conn;
    }

    // Función de ejecucióin de query
    function Select($query) {
        $conx = $this->Conn();
        $recordset = array();
        //$recordset["records"] = array();
        $read = $conx->prepare($query);
        $read->execute();
        $num = $read->rowCount();
        if ($num > 0) {
            while ($row = $read->fetch(PDO::FETCH_ASSOC)) {
                if ($row <> null) {
                    array_push($recordset, $row);
                }
            }
            // show products data in json format
            //$recordset = $read->fetchAll();
            return $recordset;
        } else {
            $read = array("err" => "Error en la consulta");
            return $read;
        }
    }

    //Maneja el select de acceso
    function SelectUser($query) {
        $conx = $this->Conn();
        $recordset = array();
        //$recordset["records"] = array();
        $read = $conx->prepare($query);
        $read->execute();
        $num = $read->rowCount();
        if ($num > 0) {
            while ($row = $read->fetch(PDO::FETCH_ASSOC)) {
                if ($row <> null) {
                    array_push($recordset, $row);
                }
            }
            // show products data in json format
            return $recordset;
        } else {
            return array("message" => "Usuario no valido");
        }
    }
}
?>
