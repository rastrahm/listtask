--
-- PostgreSQL database dump
--

-- Dumped from database version 12.6 (Ubuntu 12.6-1.pgdg20.04+1)
-- Dumped by pg_dump version 13.2 (Ubuntu 13.2-1.pgdg20.04+1)

-- Started on 2021-03-07 18:05:00 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 83604)
-- Name: program; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA program;


ALTER SCHEMA program OWNER TO postgres;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3203 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 230 (class 1255 OID 83605)
-- Name: armor(bytea); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.armor(bytea) RETURNS text
    LANGUAGE c IMMUTABLE STRICT PARALLEL SAFE
    AS '$libdir/pgcrypto', 'pg_armor';


ALTER FUNCTION public.armor(bytea) OWNER TO postgres;

--
-- TOC entry 231 (class 1255 OID 83606)
-- Name: armor(bytea, text[], text[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.armor(bytea, text[], text[]) RETURNS text
    LANGUAGE c IMMUTABLE STRICT PARALLEL SAFE
    AS '$libdir/pgcrypto', 'pg_armor';


ALTER FUNCTION public.armor(bytea, text[], text[]) OWNER TO postgres;

--
-- TOC entry 232 (class 1255 OID 83607)
-- Name: crypt(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.crypt(text, text) RETURNS text
    LANGUAGE c IMMUTABLE STRICT PARALLEL SAFE
    AS '$libdir/pgcrypto', 'pg_crypt';


ALTER FUNCTION public.crypt(text, text) OWNER TO postgres;

--
-- TOC entry 233 (class 1255 OID 83608)
-- Name: dearmor(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.dearmor(text) RETURNS bytea
    LANGUAGE c IMMUTABLE STRICT PARALLEL SAFE
    AS '$libdir/pgcrypto', 'pg_dearmor';


ALTER FUNCTION public.dearmor(text) OWNER TO postgres;

--
-- TOC entry 234 (class 1255 OID 83609)
-- Name: decrypt(bytea, bytea, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.decrypt(bytea, bytea, text) RETURNS bytea
    LANGUAGE c IMMUTABLE STRICT PARALLEL SAFE
    AS '$libdir/pgcrypto', 'pg_decrypt';


ALTER FUNCTION public.decrypt(bytea, bytea, text) OWNER TO postgres;

--
-- TOC entry 235 (class 1255 OID 83610)
-- Name: decrypt_iv(bytea, bytea, bytea, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.decrypt_iv(bytea, bytea, bytea, text) RETURNS bytea
    LANGUAGE c IMMUTABLE STRICT PARALLEL SAFE
    AS '$libdir/pgcrypto', 'pg_decrypt_iv';


ALTER FUNCTION public.decrypt_iv(bytea, bytea, bytea, text) OWNER TO postgres;

--
-- TOC entry 236 (class 1255 OID 83611)
-- Name: digest(bytea, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.digest(bytea, text) RETURNS bytea
    LANGUAGE c IMMUTABLE STRICT PARALLEL SAFE
    AS '$libdir/pgcrypto', 'pg_digest';


ALTER FUNCTION public.digest(bytea, text) OWNER TO postgres;

--
-- TOC entry 237 (class 1255 OID 83612)
-- Name: digest(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.digest(text, text) RETURNS bytea
    LANGUAGE c IMMUTABLE STRICT PARALLEL SAFE
    AS '$libdir/pgcrypto', 'pg_digest';


ALTER FUNCTION public.digest(text, text) OWNER TO postgres;

--
-- TOC entry 238 (class 1255 OID 83613)
-- Name: encryp_password(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.encryp_password() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        -- Check that empname and salary are given
        IF NEW.psw_pass IS NULL THEN
            RAISE EXCEPTION 'La clave no puede ser nula';
        END IF;
        
        -- Remember who changed the payroll when
        NEW.psw_date := current_timestamp;
        NEW.psw_pass := encrypt(NEW.psw_pass,'prueba', 'bf');
        RETURN NEW;
    END;
$$;


ALTER FUNCTION public.encryp_password() OWNER TO postgres;

--
-- TOC entry 239 (class 1255 OID 83614)
-- Name: encrypt(bytea, bytea, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.encrypt(bytea, bytea, text) RETURNS bytea
    LANGUAGE c IMMUTABLE STRICT PARALLEL SAFE
    AS '$libdir/pgcrypto', 'pg_encrypt';


ALTER FUNCTION public.encrypt(bytea, bytea, text) OWNER TO postgres;

--
-- TOC entry 240 (class 1255 OID 83615)
-- Name: encrypt_iv(bytea, bytea, bytea, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.encrypt_iv(bytea, bytea, bytea, text) RETURNS bytea
    LANGUAGE c IMMUTABLE STRICT PARALLEL SAFE
    AS '$libdir/pgcrypto', 'pg_encrypt_iv';


ALTER FUNCTION public.encrypt_iv(bytea, bytea, bytea, text) OWNER TO postgres;

--
-- TOC entry 241 (class 1255 OID 83616)
-- Name: fnc_dictionary_get_all(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_dictionary_get_all() RETURNS TABLE(str_name character varying, str_value character varying)
    LANGUAGE plpgsql
    AS $$ 	
BEGIN
	RETURN QUERY
	SELECT 
		dic_name AS str_name,
		dic_value AS str_value
	FROM 
		program.dictionary;
END
 $$;


ALTER FUNCTION public.fnc_dictionary_get_all() OWNER TO postgres;

--
-- TOC entry 242 (class 1255 OID 83617)
-- Name: fnc_get_parameter(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_get_parameter(_key character varying) RETURNS TABLE(valor text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		par_value AS valor
	FROM 
		program.parameter 
	WHERE 
		par_name = _key;
END
$$;


ALTER FUNCTION public.fnc_get_parameter(_key character varying) OWNER TO postgres;

--
-- TOC entry 243 (class 1255 OID 83618)
-- Name: fnc_grouptask_get(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_grouptask_get(_id bigint) RETURNS TABLE(big_id bigint, txt_group text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF (SELECT COUNT(usr_id) > 0 FROM public.grouptask WHERE usr_id = _id) THEN 
		BEGIN
			RETURN QUERY
			SELECT 
				usr_id AS big_id, 
				"json" AS txt_group
			FROM 
				public.grouptask 
			WHERE 
				usr_id = _id;
		END;
	ELSE
		BEGIN
			RETURN QUERY
			SELECT 
				_id AS big_id, 
				'{}' AS txt_group;
		END;
	END IF;
END
$$;


ALTER FUNCTION public.fnc_grouptask_get(_id bigint) OWNER TO postgres;

--
-- TOC entry 244 (class 1255 OID 83619)
-- Name: fnc_grouptask_get_all(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_grouptask_get_all(_id bigint) RETURNS TABLE(big_id bigint, txt_group text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		usr_id AS big_id,
		"json" AS txt_group
	FROM 
		public.grouptask 
	WHERE 
		usr_id = _id;
END
$$;


ALTER FUNCTION public.fnc_grouptask_get_all(_id bigint) OWNER TO postgres;

--
-- TOC entry 245 (class 1255 OID 83620)
-- Name: fnc_grouptask_post(bigint, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_grouptask_post(_id bigint, _json text) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO
		public.grouptask (
			usr_id,
			json
		) VALUES (
			_id,
			_json
		);
	RETURN QUERY
	SELECT 't' AS RESULT;
END
$$;


ALTER FUNCTION public.fnc_grouptask_post(_id bigint, _json text) OWNER TO postgres;

--
-- TOC entry 246 (class 1255 OID 83621)
-- Name: fnc_grouptask_put(bigint, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_grouptask_put(_id bigint, _json text) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE
		public.grouptask 
	SET 
		json = _json
	WHERE 
		usr_id =_id;
	RETURN QUERY
	SELECT 't' AS RESULT;
END
$$;


ALTER FUNCTION public.fnc_grouptask_put(_id bigint, _json text) OWNER TO postgres;

--
-- TOC entry 289 (class 1255 OID 201565)
-- Name: fnc_listtask_get(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_listtask_get(_id bigint) RETURNS TABLE(big_id bigint, txt_task text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		grouptask.usr_id as big_id ,
		grouptask.json as txt_task
	FROM 
		grouptask 
	WHERE 
		usr_id =  _id;
END
$$;


ALTER FUNCTION public.fnc_listtask_get(_id bigint) OWNER TO postgres;

--
-- TOC entry 247 (class 1255 OID 83623)
-- Name: fnc_listtask_post(bigint, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_listtask_post(_id bigint, _value text) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
DECLARE total INT;
BEGIN
	SELECT count(usr_id) INTO total FROM grouptask WHERE usr_id = _id;
	IF total > 0 THEN 
		UPDATE grouptask SET json = _value WHERE usr_id = _id;
	ELSE
		INSERT INTO grouptask (usr_id, json) VALUES(_id, _value);
	END IF;
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_listtask_post(_id bigint, _value text) OWNER TO postgres;

--
-- TOC entry 248 (class 1255 OID 83624)
-- Name: fnc_men(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_men(_prof bigint) RETURNS TABLE(big_id bigint, str_menu character varying, txt_description text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		men_id as big_id, 
		men_name as str_menu, 
		men_description as txt_descripcion 
	FROM 
		program.menu 
	WHERE 
		pro_id = _prof
	ORDER BY
		men_id ASC;
END
$$;


ALTER FUNCTION public.fnc_men(_prof bigint) OWNER TO postgres;

--
-- TOC entry 249 (class 1255 OID 83625)
-- Name: fnc_men_obj(bigint, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_men_obj(_prof bigint, _men character varying) RETURNS TABLE(str_object character varying, str_data_function character varying, str_data_parameters character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		program.menu_options.mop_object as str_object, 
		program.menu_options.mop_data_function as str_data_function,
		program.menu_options.mop_data_parameters::varchar as str_data_parameters
	FROM 
		program.menu_options 
	WHERE 
		program.menu_options.pro_id = _prof AND
		program.menu_options.mop_action = _men AND
		program.menu_options.mop_active = TRUE
	ORDER BY
		men_id ASC;
END
$$;


ALTER FUNCTION public.fnc_men_obj(_prof bigint, _men character varying) OWNER TO postgres;

--
-- TOC entry 250 (class 1255 OID 83626)
-- Name: fnc_men_sub(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_men_sub(_prof bigint, _men bigint) RETURNS TABLE(big_id bigint, big_men_id bigint, str_menu character varying, txt_description text, str_action character varying, str_shortcut character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		program.menu_options.mop_id as big_id, 
		program.menu_options.men_id as big_men_id,
		program.menu_options.mop_name as str_menu, 
		program.menu_options.mop_description as txt_description,
		program.menu_options.mop_action as str_action,
		program.menu_options.mop_shortcut as str_shortcut
	FROM 
		program.menu_options 
	WHERE 
		program.menu_options.pro_id = _prof AND
		program.menu_options.men_id = _men AND
		program.menu_options.mop_active = TRUE
	ORDER BY
		men_id, mop_id ASC;
END
$$;


ALTER FUNCTION public.fnc_men_sub(_prof bigint, _men bigint) OWNER TO postgres;

--
-- TOC entry 251 (class 1255 OID 83627)
-- Name: fnc_parameter_delete(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_parameter_delete(_id character varying) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
declare
	id_array int[];
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Eliminación de parametro: ', _id, ', ', _id)
		);
	id_array := (string_to_array(_id, ',')::int[]);
	DELETE FROM
		program.parameter 
	WHERE
		cast(par_id as int) = any (id_array);
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_parameter_delete(_id character varying) OWNER TO postgres;

--
-- TOC entry 252 (class 1255 OID 83628)
-- Name: fnc_parameter_get(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_parameter_get(_id bigint) RETURNS TABLE(big_id bigint, str_name character varying, txt_value text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		par_id AS big_id,
		par_name AS str_name,
		par_value AS txt_value
	FROM 
		program.parameter 
	WHERE 
		par_id = _id;
END
$$;


ALTER FUNCTION public.fnc_parameter_get(_id bigint) OWNER TO postgres;

--
-- TOC entry 265 (class 1255 OID 83629)
-- Name: fnc_parameter_get_all(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_parameter_get_all() RETURNS TABLE(big_id bigint, str_name character varying, txt_value text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		par_id AS big_id,
		par_name AS str_name,
		par_value AS txt_value
	FROM 
		program.parameter 
	WHERE 
		par_visible = 1;
END
$$;


ALTER FUNCTION public.fnc_parameter_get_all() OWNER TO postgres;

--
-- TOC entry 266 (class 1255 OID 83630)
-- Name: fnc_parameter_post(character varying, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_parameter_post(_name character varying, _value text) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Inserción de parametro: ', _name, ', ', _value)
		);
	INSERT INTO
		program.parameter (
		par_name,
		par_value,
		par_visible
		)
	VALUES (
		_name,
		_value,
		1
		);
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_parameter_post(_name character varying, _value text) OWNER TO postgres;

--
-- TOC entry 267 (class 1255 OID 83631)
-- Name: fnc_parameter_put(bigint, character varying, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_parameter_put(_id bigint, _name character varying, _value text) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Cambio de parametro: ', _id, ', ', _name, ' de valor ', (select par_value from program.parameter where par_id = _id), ' a ', _value)
		);
	UPDATE 
		program.parameter 
	SET
		par_name = _name,
		par_value = _value
	WHERE
		par_id = _id;
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_parameter_put(_id bigint, _name character varying, _value text) OWNER TO postgres;

--
-- TOC entry 268 (class 1255 OID 83632)
-- Name: fnc_parameter_value(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_parameter_value(_name character varying) RETURNS TABLE(txt_value text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		par_value AS txt_value
	FROM 
		program.parameter 
	WHERE 
		par_name = _name;
END
$$;


ALTER FUNCTION public.fnc_parameter_value(_name character varying) OWNER TO postgres;

--
-- TOC entry 269 (class 1255 OID 83633)
-- Name: fnc_previews_delete(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_previews_delete(_id character varying) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
declare
	id_array int[];
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Eliminación de articulo: ', _id)
		);
	id_array := (string_to_array(_id, ',')::int[]);
	delete FROM
		program.preview 
	where
		cast(pre_id as int) = any (id_array);
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_previews_delete(_id character varying) OWNER TO postgres;

--
-- TOC entry 270 (class 1255 OID 83634)
-- Name: fnc_previews_get(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_previews_get(_id bigint) RETURNS TABLE(big_id bigint, str_link character varying, txt_title text, txt_article text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		pre_id AS big_id,
		pre_link as str_link,
		pre_title AS txt_title,
		pre_article AS txt_article
	FROM 
		program.preview
	WHERE
		pre_id = _id;
END
$$;


ALTER FUNCTION public.fnc_previews_get(_id bigint) OWNER TO postgres;

--
-- TOC entry 271 (class 1255 OID 83635)
-- Name: fnc_previews_get(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_previews_get(_link character varying) RETURNS TABLE(big_id bigint, str_link character varying, txt_title text, txt_article text)
    LANGUAGE plpgsql
    AS $$
begin
	RETURN QUERY
	SELECT 
		pre_id AS big_id_id,
		pre_link AS str_link,
		pre_title AS txt_title,
		pre_article AS txt_article
	FROM 
		program.preview
	WHERE
		lower(pre_link) = lower(_link);
END
$$;


ALTER FUNCTION public.fnc_previews_get(_link character varying) OWNER TO postgres;

--
-- TOC entry 272 (class 1255 OID 83636)
-- Name: fnc_previews_get_all(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_previews_get_all() RETURNS TABLE(big_id bigint, str_link character varying, txt_title text, txt_article text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		pre_id AS big_id,
		pre_link AS str_link,
		pre_title AS txt_title,
		pre_article AS txt_article
	FROM 
		program.preview
	ORDER BY pre_id;
END
$$;


ALTER FUNCTION public.fnc_previews_get_all() OWNER TO postgres;

--
-- TOC entry 273 (class 1255 OID 83637)
-- Name: fnc_previews_get_list(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_previews_get_list() RETURNS TABLE(big_id bigint, str_link character varying)
    LANGUAGE plpgsql
    AS $$ 	
BEGIN
	RETURN QUERY
	SELECT 
		pre_id AS big_id,
		pre_link AS txt_link
	FROM 
		program.preview;
END
$$;


ALTER FUNCTION public.fnc_previews_get_list() OWNER TO postgres;

--
-- TOC entry 274 (class 1255 OID 83638)
-- Name: fnc_previews_post(character varying, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_previews_post(_link character varying, _title text, _article text) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Inserción de articulo: ', _link, ', ', _title, ', ', _article)
		);
	INSERT INTO
		program.preview (
			pre_id,
			pre_link,
			pre_title,
			pre_article
		)
	VALUES (
		nextval('preview_pre_id_seq'),
		_link,
		_title,
		_article
		);
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_previews_post(_link character varying, _title text, _article text) OWNER TO postgres;

--
-- TOC entry 275 (class 1255 OID 83639)
-- Name: fnc_previews_put(bigint, character varying, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_previews_put(_id bigint, _link character varying, _title text, _article text) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Edición de articulo: ', _id, ' de: ', (select pre_link from program.preview where pre_id = _id), ' a: ' , _link, '; ', (select pre_title from program.preview where pre_id = _id), ' a: ' , _title, ', de: ', (select pre_article from program.preview where pre_id = _id), ' a: ', _article)
		);
	UPDATE
		program.preview 
	set 
		pre_link = _link,
		pre_title = _title,
		pre_article = _article
	where
		pre_id = _id;
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_previews_put(_id bigint, _link character varying, _title text, _article text) OWNER TO postgres;

--
-- TOC entry 276 (class 1255 OID 83640)
-- Name: fnc_profile_get(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_profile_get(_pro bigint) RETURNS TABLE(big_id bigint, str_name character varying, big_menu_id bigint, str_menu_name character varying, big_profile_id bigint, str_profile_name character varying, txt_profile_description text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		program.profile.pro_id as big_id,
		program.profile.pro_name as str_name,
		program.menu.men_id as big_menu_id,
		program.menu.men_name as str_menu_name,
		program.menu_options.mop_id as big_profile_id,
		program.menu_options.mop_name as str_profile_name,
		program.menu_options.mop_description as txt_profile_description 
	FROM 
		program.profile 
		INNER JOIN program.menu ON program.menu.pro_id = program.profile.pro_id
		INNER JOIN program.menu_options ON program.menu_options.men_id = program.menu.men_id
	WHERE
		program.profile.pro_id = _pro
	ORDER BY
		program.menu_options.men_id,
		program.menu_options.mop_id;
END
$$;


ALTER FUNCTION public.fnc_profile_get(_pro bigint) OWNER TO postgres;

--
-- TOC entry 277 (class 1255 OID 83641)
-- Name: fnc_profile_get_all(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_profile_get_all() RETURNS TABLE(big_id bigint, str_name character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		program.profile.pro_id as big_id,
		program.profile.pro_name as str_name
	FROM 
		program.profile;
END
$$;


ALTER FUNCTION public.fnc_profile_get_all() OWNER TO postgres;

--
-- TOC entry 278 (class 1255 OID 83642)
-- Name: fnc_table_descript(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_table_descript(_table character varying) RETURNS TABLE(str_column character varying, str_type character varying, int_length integer)
    LANGUAGE plpgsql
    AS $$ 	
BEGIN
	RETURN QUERY
	select 
		CAST(column_name as varchar) as str_column, 
		CAST(data_type as varchar) as str_type, 
		CAST(character_maximum_length as int) as int_length
	from 
		INFORMATION_SCHEMA.COLUMNS 
	where table_name = _table;
END
 $$;


ALTER FUNCTION public.fnc_table_descript(_table character varying) OWNER TO postgres;

--
-- TOC entry 279 (class 1255 OID 83643)
-- Name: fnc_user_activate(bigint, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_user_activate(_id bigint, _user character varying) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO
		program.log (
		log_date,
		log_time,
		log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Activación de usuario ', _user)
		);
	UPDATE 
		"program"."user"
	SET
		usr_active = 1
	WHERE 
		usr_id = _id;
	RETURN QUERY
	SELECT 't'  AS result;
END
$$;


ALTER FUNCTION public.fnc_user_activate(_id bigint, _user character varying) OWNER TO postgres;

--
-- TOC entry 280 (class 1255 OID 83644)
-- Name: fnc_user_pass(character varying, bytea); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_user_pass(_user character varying, _pass bytea) RETURNS TABLE(big_id bigint, big_profile bigint)
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO
		program.log (
		log_date,
		log_time,
		log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Acceso al sistema usuario ', _user)
		);
	RETURN QUERY
	SELECT
		program.user.usr_id as big_id, 
		program.user.pro_id as big_profile
	FROM
		program.user
		INNER JOIN program.user_pass ON program.user.usr_id = program.user_pass.usr_id
	WHERE
		program.user.usr_name = _user AND
		program.user_pass.psw_pass =  _pass
	LIMIT 1;
END
$$;


ALTER FUNCTION public.fnc_user_pass(_user character varying, _pass bytea) OWNER TO postgres;

--
-- TOC entry 281 (class 1255 OID 83645)
-- Name: fnc_user_verify(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_user_verify(_id integer, _user character varying) RETURNS TABLE(result boolean)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT
		CASE WHEN COUNT(usr_id) > 0 THEN true ELSE false END AS result 
	FROM
		program.user
	WHERE
		program.user.usr_id = _id AND
		program.user.usr_name =  _user
	LIMIT 1;
END
$$;


ALTER FUNCTION public.fnc_user_verify(_id integer, _user character varying) OWNER TO postgres;

--
-- TOC entry 282 (class 1255 OID 83646)
-- Name: fnc_users_delete(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_users_delete(_id character varying) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
declare
	id_array int[];
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Eliminación de Usuario: ', _id)
		);
	id_array := (string_to_array(_id, ',')::int[]);
	DELETE FROM
		program.user 
	where
		cast(usr_id as int) = any (id_array);
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_users_delete(_id character varying) OWNER TO postgres;

--
-- TOC entry 283 (class 1255 OID 83647)
-- Name: fnc_users_get(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_users_get(_id bigint) RETURNS TABLE(big_id bigint, str_name character varying, bol_active smallint, int_pro bigint, str_pro_name character varying, str_email character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		program.user.usr_id AS big_id,
		usr_name AS str_name,
		usr_active AS bol_active,
		program.user.pro_id AS big_pro,
		pro_name AS str_pro_name,
		public.user_detail.usr_email AS str_email
	FROM 
		program.user  
		INNER JOIN program.profile ON program.profile.pro_id = program.user.pro_id
		INNER JOIN public.user_detail ON program."user".usr_id = public.user_detail.usr_id 
	WHERE 
		program.USER.usr_id = _id;
END
$$;


ALTER FUNCTION public.fnc_users_get(_id bigint) OWNER TO postgres;

--
-- TOC entry 284 (class 1255 OID 83648)
-- Name: fnc_users_get_all(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_users_get_all() RETURNS TABLE(big_id bigint, str_name character varying, bol_active smallint, big_profile bigint, str_profile_name character varying, str_email character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		program.user.usr_id AS big_id,
		usr_name AS str_name,
		usr_active AS bol_active,
		program.user.pro_id AS big_profile,
		pro_name AS str_profile_name,
		public.user_detail.usr_email AS str_email
	FROM 
		program.user  
		INNER JOIN program.profile ON program.profile.pro_id = program.user.pro_id
		INNER JOIN public.user_detail ON program.user.usr_id = public.user_detail.usr_id;
END
$$;


ALTER FUNCTION public.fnc_users_get_all() OWNER TO postgres;

--
-- TOC entry 285 (class 1255 OID 83649)
-- Name: fnc_users_get_pass(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_users_get_pass(_email character varying) RETURNS TABLE(str_user character varying, str_pass text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		"program"."user".usr_name AS str_user,
		"program".decrypt(psw_pass, CAST((SELECT par_value FROM "program"."parameter" WHERE par_name = 'JWT_KEY') AS bytea), 'aes') AS str_pass
	FROM 
		program.user_pass
		INNER JOIN "program"."user" ON "program"."user".usr_id = "program"."user_pass".usr_id
	WHERE 
		program.user_pass.usr_id = (SELECT usr_id FROM public.user_detail WHERE usr_email = _email);
END
$$;


ALTER FUNCTION public.fnc_users_get_pass(_email character varying) OWNER TO postgres;

--
-- TOC entry 286 (class 1255 OID 83650)
-- Name: fnc_users_mail_send(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_users_mail_send(_id bigint) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE 
		public.user_detail 
	SET
		email_enviado = TRUE 
	WHERE 
		usr_id = _id;
	RETURN QUERY
	SELECT 't'  AS result;
END
$$;


ALTER FUNCTION public.fnc_users_mail_send(_id bigint) OWNER TO postgres;

--
-- TOC entry 287 (class 1255 OID 83651)
-- Name: fnc_users_post(character varying, smallint, bigint, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_users_post(_name character varying, _active smallint, _pro bigint, _email character varying, _pass character varying) RETURNS TABLE(result text, id bigint)
    LANGUAGE plpgsql
    AS $$
DECLARE usr BIGINT;
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		) VALUES (
			CURRENT_DATE,
			CURRENT_TIME,
			CONCAT('Inserción de Usuario: ', _name, ', ', _pro)
		);
	INSERT INTO
		program.user (
			usr_id,
			usr_name,
			usr_active,
			pro_id
		) VALUES (
			nextval('program.user_id_usr_seq'),
			_name,
			_active,
			_pro
		)
	RETURNING usr_id INTO usr;
	INSERT INTO 
		public.user_detail (
			usr_id ,
			usr_email 
		) VALUES (
			usr,
			_email
		);
	INSERT INTO
		"program".user_pass (
			usr_id,
			psw_date,
			psw_pass 
		) VALUES (
			usr,
			Now(),
			"program".encrypt(CAST(_pass AS bytea), CAST((SELECT par_value FROM "program"."parameter" WHERE par_name = 'JWT_KEY') AS bytea), 'aes')
		);
	RETURN QUERY
	SELECT 't', usr AS result;
END
$$;


ALTER FUNCTION public.fnc_users_post(_name character varying, _active smallint, _pro bigint, _email character varying, _pass character varying) OWNER TO postgres;

--
-- TOC entry 288 (class 1255 OID 83652)
-- Name: fnc_users_put(bigint, character varying, smallint, bigint, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_users_put(_id bigint, _name character varying, _act smallint, _pro bigint, _email character varying) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Edición de Usuario: ', _id, ', ', _name, ', ', _act, ', ', _pro)
		);
	UPDATE 
		program.user 
	set 
		usr_name = _name,
		usr_active = _act,
		pro_id = _pro
	where
		usr_id = _id;
	UPDATE 
		public.user_detail 
	SET 
		usr_email = _email
	WHERE 
		usr_id = _id;		
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_users_put(_id bigint, _name character varying, _act smallint, _pro bigint, _email character varying) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 203 (class 1259 OID 83653)
-- Name: dictionary; Type: TABLE; Schema: program; Owner: postgres
--

CREATE TABLE program.dictionary (
    dic_id bigint NOT NULL,
    dic_name character varying(50),
    dic_value character varying(50)
);


ALTER TABLE program.dictionary OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 83656)
-- Name: dictionary_dic_id_seq; Type: SEQUENCE; Schema: program; Owner: postgres
--

CREATE SEQUENCE program.dictionary_dic_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.dictionary_dic_id_seq OWNER TO postgres;

--
-- TOC entry 3204 (class 0 OID 0)
-- Dependencies: 204
-- Name: dictionary_dic_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: postgres
--

ALTER SEQUENCE program.dictionary_dic_id_seq OWNED BY program.dictionary.dic_id;


--
-- TOC entry 205 (class 1259 OID 83658)
-- Name: log; Type: TABLE; Schema: program; Owner: postgres
--

CREATE TABLE program.log (
    log_id bigint NOT NULL,
    log_date date,
    log_time time without time zone,
    log_event text
);


ALTER TABLE program.log OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 83664)
-- Name: log_log_id_seq; Type: SEQUENCE; Schema: program; Owner: postgres
--

CREATE SEQUENCE program.log_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.log_log_id_seq OWNER TO postgres;

--
-- TOC entry 3205 (class 0 OID 0)
-- Dependencies: 206
-- Name: log_log_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: postgres
--

ALTER SEQUENCE program.log_log_id_seq OWNED BY program.log.log_id;


--
-- TOC entry 207 (class 1259 OID 83666)
-- Name: menu; Type: TABLE; Schema: program; Owner: rasg
--

CREATE TABLE program.menu (
    men_id bigint NOT NULL,
    pro_id bigint NOT NULL,
    men_name character varying(15),
    men_description text
);


ALTER TABLE program.menu OWNER TO rasg;

--
-- TOC entry 3206 (class 0 OID 0)
-- Dependencies: 207
-- Name: TABLE menu; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON TABLE program.menu IS 'Menús del nivel principal';


--
-- TOC entry 3207 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN menu.pro_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu.pro_id IS 'Id del perfil correspondiente';


--
-- TOC entry 3208 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN menu.men_name; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu.men_name IS 'Nombre del menú';


--
-- TOC entry 3209 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN menu.men_description; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu.men_description IS 'Descripción del menú';


--
-- TOC entry 208 (class 1259 OID 83672)
-- Name: menu_men_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.menu_men_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.menu_men_id_seq OWNER TO rasg;

--
-- TOC entry 3210 (class 0 OID 0)
-- Dependencies: 208
-- Name: menu_men_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.menu_men_id_seq OWNED BY program.menu.men_id;


--
-- TOC entry 209 (class 1259 OID 83674)
-- Name: menu_options; Type: TABLE; Schema: program; Owner: rasg
--

CREATE TABLE program.menu_options (
    mop_id bigint NOT NULL,
    men_id bigint NOT NULL,
    pro_id bigint NOT NULL,
    mop_name character varying(15),
    mop_description text,
    mop_shortcut character varying(10),
    mop_active boolean DEFAULT true,
    mop_object character varying(25),
    mop_data_function character varying(50),
    mop_data_parameters character varying(25)[],
    mop_action character varying(15)
);


ALTER TABLE program.menu_options OWNER TO rasg;

--
-- TOC entry 3211 (class 0 OID 0)
-- Dependencies: 209
-- Name: TABLE menu_options; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON TABLE program.menu_options IS 'Submenús';


--
-- TOC entry 3212 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN menu_options.mop_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.mop_id IS 'Id de los submenús';


--
-- TOC entry 3213 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN menu_options.men_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.men_id IS 'Menú al que pertenece';


--
-- TOC entry 3214 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN menu_options.pro_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.pro_id IS 'Id del perfil al que pertenece';


--
-- TOC entry 3215 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN menu_options.mop_name; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.mop_name IS 'Nombre del submenu';


--
-- TOC entry 3216 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN menu_options.mop_description; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.mop_description IS 'Descripción del submenú';


--
-- TOC entry 3217 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN menu_options.mop_shortcut; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.mop_shortcut IS 'Teclas del acceso directo';


--
-- TOC entry 3218 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN menu_options.mop_object; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.mop_object IS 'Objeto de uso';


--
-- TOC entry 3219 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN menu_options.mop_data_function; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.mop_data_function IS 'Funcion en la base de datos';


--
-- TOC entry 210 (class 1259 OID 83681)
-- Name: menu_options_men_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.menu_options_men_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.menu_options_men_id_seq OWNER TO rasg;

--
-- TOC entry 3220 (class 0 OID 0)
-- Dependencies: 210
-- Name: menu_options_men_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.menu_options_men_id_seq OWNED BY program.menu_options.men_id;


--
-- TOC entry 211 (class 1259 OID 83683)
-- Name: menu_options_mop_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.menu_options_mop_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.menu_options_mop_id_seq OWNER TO rasg;

--
-- TOC entry 3221 (class 0 OID 0)
-- Dependencies: 211
-- Name: menu_options_mop_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.menu_options_mop_id_seq OWNED BY program.menu_options.mop_id;


--
-- TOC entry 212 (class 1259 OID 83685)
-- Name: menu_options_pro_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.menu_options_pro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.menu_options_pro_id_seq OWNER TO rasg;

--
-- TOC entry 3222 (class 0 OID 0)
-- Dependencies: 212
-- Name: menu_options_pro_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.menu_options_pro_id_seq OWNED BY program.menu_options.pro_id;


--
-- TOC entry 213 (class 1259 OID 83687)
-- Name: menu_pro_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.menu_pro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.menu_pro_id_seq OWNER TO rasg;

--
-- TOC entry 3223 (class 0 OID 0)
-- Dependencies: 213
-- Name: menu_pro_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.menu_pro_id_seq OWNED BY program.menu.pro_id;


--
-- TOC entry 214 (class 1259 OID 83689)
-- Name: parameter; Type: TABLE; Schema: program; Owner: postgres
--

CREATE TABLE program.parameter (
    par_id bigint NOT NULL,
    par_name character varying(15),
    par_value text,
    par_visible smallint DEFAULT 0 NOT NULL
);


ALTER TABLE program.parameter OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 83696)
-- Name: parameter_par_id_seq; Type: SEQUENCE; Schema: program; Owner: postgres
--

CREATE SEQUENCE program.parameter_par_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.parameter_par_id_seq OWNER TO postgres;

--
-- TOC entry 3224 (class 0 OID 0)
-- Dependencies: 215
-- Name: parameter_par_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: postgres
--

ALTER SEQUENCE program.parameter_par_id_seq OWNED BY program.parameter.par_id;


--
-- TOC entry 216 (class 1259 OID 83698)
-- Name: preview; Type: TABLE; Schema: program; Owner: rasg
--

CREATE TABLE program.preview (
    pre_id bigint NOT NULL,
    pre_title text,
    pre_article text,
    pre_link character varying(50)
);


ALTER TABLE program.preview OWNER TO rasg;

--
-- TOC entry 3226 (class 0 OID 0)
-- Dependencies: 216
-- Name: COLUMN preview.pre_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.preview.pre_id IS 'Id interno de la tabla preview';


--
-- TOC entry 3227 (class 0 OID 0)
-- Dependencies: 216
-- Name: COLUMN preview.pre_title; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.preview.pre_title IS 'Titulo del articulo';


--
-- TOC entry 3228 (class 0 OID 0)
-- Dependencies: 216
-- Name: COLUMN preview.pre_article; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.preview.pre_article IS 'Articulo en HTML';


--
-- TOC entry 217 (class 1259 OID 83704)
-- Name: preview_pre_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.preview_pre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.preview_pre_id_seq OWNER TO rasg;

--
-- TOC entry 3229 (class 0 OID 0)
-- Dependencies: 217
-- Name: preview_pre_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.preview_pre_id_seq OWNED BY program.preview.pre_id;


--
-- TOC entry 218 (class 1259 OID 83706)
-- Name: profile; Type: TABLE; Schema: program; Owner: rasg
--

CREATE TABLE program.profile (
    pro_id bigint NOT NULL,
    pro_name character varying(15)
);


ALTER TABLE program.profile OWNER TO rasg;

--
-- TOC entry 3230 (class 0 OID 0)
-- Dependencies: 218
-- Name: COLUMN profile.pro_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.profile.pro_id IS 'Id del perfil';


--
-- TOC entry 3231 (class 0 OID 0)
-- Dependencies: 218
-- Name: COLUMN profile.pro_name; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.profile.pro_name IS 'Nombre del perfil';


--
-- TOC entry 219 (class 1259 OID 83709)
-- Name: profile_pro_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.profile_pro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.profile_pro_id_seq OWNER TO rasg;

--
-- TOC entry 3232 (class 0 OID 0)
-- Dependencies: 219
-- Name: profile_pro_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.profile_pro_id_seq OWNED BY program.profile.pro_id;


--
-- TOC entry 220 (class 1259 OID 83711)
-- Name: user; Type: TABLE; Schema: program; Owner: rasg
--

CREATE TABLE program."user" (
    usr_id bigint NOT NULL,
    usr_name character varying(20) NOT NULL,
    usr_active smallint DEFAULT 0 NOT NULL,
    pro_id bigint NOT NULL
);


ALTER TABLE program."user" OWNER TO rasg;

--
-- TOC entry 3233 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN "user".usr_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program."user".usr_id IS 'Id de usuario';


--
-- TOC entry 3234 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN "user".usr_name; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program."user".usr_name IS 'Usuario del sistema';


--
-- TOC entry 3235 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN "user".usr_active; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program."user".usr_active IS '0 Usuario Inactivo; 1 Usuario Activo';


--
-- TOC entry 3236 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN "user".pro_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program."user".pro_id IS 'Id del perfil';


--
-- TOC entry 221 (class 1259 OID 83715)
-- Name: user_id_usr_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.user_id_usr_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.user_id_usr_seq OWNER TO rasg;

--
-- TOC entry 3237 (class 0 OID 0)
-- Dependencies: 221
-- Name: user_id_usr_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.user_id_usr_seq OWNED BY program."user".usr_id;


--
-- TOC entry 222 (class 1259 OID 83717)
-- Name: user_pass; Type: TABLE; Schema: program; Owner: rasg
--

CREATE TABLE program.user_pass (
    psw_id bigint NOT NULL,
    usr_id bigint NOT NULL,
    psw_date timestamp without time zone NOT NULL,
    psw_pass bytea
);


ALTER TABLE program.user_pass OWNER TO rasg;

--
-- TOC entry 3238 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN user_pass.psw_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.user_pass.psw_id IS 'Id de la tabla password';


--
-- TOC entry 3239 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN user_pass.usr_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.user_pass.usr_id IS 'Id del usuario';


--
-- TOC entry 3240 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN user_pass.psw_date; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.user_pass.psw_date IS 'Fecha y hora de creación del password';


--
-- TOC entry 3241 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN user_pass.psw_pass; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.user_pass.psw_pass IS 'Password cifrado';


--
-- TOC entry 223 (class 1259 OID 83723)
-- Name: user_pass_psw_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.user_pass_psw_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.user_pass_psw_id_seq OWNER TO rasg;

--
-- TOC entry 3242 (class 0 OID 0)
-- Dependencies: 223
-- Name: user_pass_psw_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.user_pass_psw_id_seq OWNED BY program.user_pass.psw_id;


--
-- TOC entry 224 (class 1259 OID 83725)
-- Name: user_pass_usr_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.user_pass_usr_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.user_pass_usr_id_seq OWNER TO rasg;

--
-- TOC entry 3243 (class 0 OID 0)
-- Dependencies: 224
-- Name: user_pass_usr_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.user_pass_usr_id_seq OWNED BY program.user_pass.usr_id;


--
-- TOC entry 225 (class 1259 OID 83727)
-- Name: user_pro_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.user_pro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.user_pro_id_seq OWNER TO rasg;

--
-- TOC entry 3244 (class 0 OID 0)
-- Dependencies: 225
-- Name: user_pro_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.user_pro_id_seq OWNED BY program."user".pro_id;


--
-- TOC entry 226 (class 1259 OID 83729)
-- Name: grouptask; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grouptask (
    usr_id bigint NOT NULL,
    json text
);


ALTER TABLE public.grouptask OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 83735)
-- Name: grouptask_iduser_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.grouptask_iduser_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grouptask_iduser_seq OWNER TO postgres;

--
-- TOC entry 3245 (class 0 OID 0)
-- Dependencies: 227
-- Name: grouptask_iduser_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.grouptask_iduser_seq OWNED BY public.grouptask.usr_id;


--
-- TOC entry 229 (class 1259 OID 83868)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 83737)
-- Name: user_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_detail (
    usr_id bigint NOT NULL,
    usr_email character varying(100),
    email_enviado boolean DEFAULT false
);


ALTER TABLE public.user_detail OWNER TO postgres;

--
-- TOC entry 2998 (class 2604 OID 83741)
-- Name: dictionary dic_id; Type: DEFAULT; Schema: program; Owner: postgres
--

ALTER TABLE ONLY program.dictionary ALTER COLUMN dic_id SET DEFAULT nextval('program.dictionary_dic_id_seq'::regclass);


--
-- TOC entry 2999 (class 2604 OID 83742)
-- Name: log log_id; Type: DEFAULT; Schema: program; Owner: postgres
--

ALTER TABLE ONLY program.log ALTER COLUMN log_id SET DEFAULT nextval('program.log_log_id_seq'::regclass);


--
-- TOC entry 3000 (class 2604 OID 83743)
-- Name: menu men_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu ALTER COLUMN men_id SET DEFAULT nextval('program.menu_men_id_seq'::regclass);


--
-- TOC entry 3001 (class 2604 OID 83744)
-- Name: menu pro_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu ALTER COLUMN pro_id SET DEFAULT nextval('program.menu_pro_id_seq'::regclass);


--
-- TOC entry 3003 (class 2604 OID 83745)
-- Name: menu_options mop_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu_options ALTER COLUMN mop_id SET DEFAULT nextval('program.menu_options_mop_id_seq'::regclass);


--
-- TOC entry 3004 (class 2604 OID 83746)
-- Name: menu_options men_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu_options ALTER COLUMN men_id SET DEFAULT nextval('program.menu_options_men_id_seq'::regclass);


--
-- TOC entry 3005 (class 2604 OID 83747)
-- Name: menu_options pro_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu_options ALTER COLUMN pro_id SET DEFAULT nextval('program.menu_options_pro_id_seq'::regclass);


--
-- TOC entry 3007 (class 2604 OID 83748)
-- Name: parameter par_id; Type: DEFAULT; Schema: program; Owner: postgres
--

ALTER TABLE ONLY program.parameter ALTER COLUMN par_id SET DEFAULT nextval('program.parameter_par_id_seq'::regclass);


--
-- TOC entry 3008 (class 2604 OID 83749)
-- Name: preview pre_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.preview ALTER COLUMN pre_id SET DEFAULT nextval('program.preview_pre_id_seq'::regclass);


--
-- TOC entry 3009 (class 2604 OID 83750)
-- Name: profile pro_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.profile ALTER COLUMN pro_id SET DEFAULT nextval('program.profile_pro_id_seq'::regclass);


--
-- TOC entry 3011 (class 2604 OID 83751)
-- Name: user usr_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program."user" ALTER COLUMN usr_id SET DEFAULT nextval('program.user_id_usr_seq'::regclass);


--
-- TOC entry 3012 (class 2604 OID 83752)
-- Name: user pro_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program."user" ALTER COLUMN pro_id SET DEFAULT nextval('program.user_pro_id_seq'::regclass);


--
-- TOC entry 3013 (class 2604 OID 83753)
-- Name: user_pass psw_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.user_pass ALTER COLUMN psw_id SET DEFAULT nextval('program.user_pass_psw_id_seq'::regclass);


--
-- TOC entry 3014 (class 2604 OID 83754)
-- Name: user_pass usr_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.user_pass ALTER COLUMN usr_id SET DEFAULT nextval('program.user_pass_usr_id_seq'::regclass);


--
-- TOC entry 3171 (class 0 OID 83653)
-- Dependencies: 203
-- Data for Name: dictionary; Type: TABLE DATA; Schema: program; Owner: postgres
--

COPY program.dictionary (dic_id, dic_name, dic_value) FROM stdin;
9	user	Usuarios
10	profile	Perfiles
12	parameter	Parametros
15	pro_id	Perfil
11	item	Vistas
1	str_name	Nombre
3	str_title	Titulo
4	str_link	Enlace
5	txt_article	Articulo
6	str_active	Activo
13	str_name	Nombre
14	bol_active	Activar
16	str_name	Nombre Perfil
17	txt_title	Titulo
18	txt_article	HTML Cuerpo
19	str_link	Enlace
2	txt_value	Valor
20	preview	Portada
21	big_id	ID
22	par_name	Nombre
23	par_value	Valor
24	par_visible	Visible
25	usr_name	Nombre Usuario
26	usr_active	Usuario Activo
27	pro_name	Nombre Perfil
28	pre_title	Titulo
29	pre_article	Articulo
30	pre_link	Enlace
8	str_pro_name	Nombre Perfil
7	int_pro	Perfil
31	big_profile	Perfil
32	str_profile_name	Nombre Perfil
33	str_email	E-Mail
34	txt_group	Tareas
\.


--
-- TOC entry 3173 (class 0 OID 83658)
-- Dependencies: 205
-- Data for Name: log; Type: TABLE DATA; Schema: program; Owner: postgres
--

COPY program.log (log_id, log_date, log_time, log_event) FROM stdin;
1	2019-07-14	18:48:28.217742	Acceso al sistema usuario admin
2	2019-08-04	12:39:55.917036	Acceso al sistema usuario admin
4	2019-08-04	13:08:38.353502	Acceso al sistema usuario admin
5	2019-08-04	13:14:29.34779	Acceso al sistema usuario admin
6	2019-08-11	13:12:51.731441	Acceso al sistema usuario admin
7	2019-08-11	13:16:08.171862	Acceso al sistema usuario admin
8	2019-08-11	13:18:39.459455	Acceso al sistema usuario admin
9	2019-08-11	13:23:17.593264	Acceso al sistema usuario admin
10	2019-08-11	13:25:36.401066	Acceso al sistema usuario admin
11	2019-08-11	13:27:31.047828	Acceso al sistema usuario admin
12	2019-08-11	13:28:21.7025	Acceso al sistema usuario admin
13	2019-08-11	13:51:32.632638	Acceso al sistema usuario admin
14	2019-08-11	14:01:26.708336	Acceso al sistema usuario admin
15	2019-08-11	14:03:22.463325	Acceso al sistema usuario admin
16	2019-08-11	14:03:41.65361	Acceso al sistema usuario admin
17	2019-08-11	14:04:20.857682	Acceso al sistema usuario admin
18	2019-08-11	14:04:23.394927	Acceso al sistema usuario admin
19	2019-08-11	14:05:04.148899	Acceso al sistema usuario admin
20	2019-08-11	14:06:57.903084	Acceso al sistema usuario admin
21	2019-08-11	14:07:25.176994	Acceso al sistema usuario admin
22	2019-08-11	14:08:23.243987	Acceso al sistema usuario admin
23	2019-08-11	14:11:01.758764	Acceso al sistema usuario admin
24	2019-08-11	14:12:02.462265	Acceso al sistema usuario admin
25	2019-08-11	14:13:55.564969	Acceso al sistema usuario admin
26	2019-08-11	14:19:36.347912	Acceso al sistema usuario admin
27	2019-08-11	14:20:02.800939	Acceso al sistema usuario admin
28	2019-08-11	14:20:23.74491	Acceso al sistema usuario admin
29	2019-08-11	14:21:25.984408	Acceso al sistema usuario admin
30	2019-08-12	20:34:17.036146	Acceso al sistema usuario admin
31	2019-08-12	20:34:30.909787	Acceso al sistema usuario admin
32	2019-08-12	20:36:30.351838	Acceso al sistema usuario admin
33	2019-08-12	20:38:27.34321	Acceso al sistema usuario admin
34	2019-08-12	20:38:47.945147	Acceso al sistema usuario admin
35	2019-08-12	20:42:24.039258	Acceso al sistema usuario admin
36	2019-08-12	20:44:29.278542	Acceso al sistema usuario admin
37	2019-08-18	09:56:24.462556	Acceso al sistema usuario admin
38	2019-08-18	10:19:37.762429	Acceso al sistema usuario admin
39	2019-08-18	10:22:14.022525	Acceso al sistema usuario admin
40	2019-08-18	10:23:09.952071	Acceso al sistema usuario admin
41	2019-08-18	10:24:53.281491	Acceso al sistema usuario admin
42	2019-08-18	10:25:26.722785	Acceso al sistema usuario admin
43	2019-08-18	10:33:27.265809	Acceso al sistema usuario admin
44	2019-08-18	10:34:23.199478	Acceso al sistema usuario 
45	2019-08-18	10:34:57.551256	Acceso al sistema usuario admin
46	2019-08-18	12:16:55.071214	Acceso al sistema usuario admin
47	2019-08-18	12:17:30.694178	Acceso al sistema usuario admin
48	2019-08-18	12:18:29.613314	Acceso al sistema usuario admin
49	2019-08-18	12:23:27.120377	Acceso al sistema usuario admin
50	2019-08-18	12:25:52.097443	Acceso al sistema usuario admin
51	2019-08-18	12:26:34.472461	Acceso al sistema usuario admin
52	2019-08-18	12:40:49.972745	Acceso al sistema usuario admin
53	2019-08-18	12:42:40.208456	Acceso al sistema usuario admin
54	2019-08-18	12:43:56.561505	Acceso al sistema usuario admin
55	2019-08-18	12:44:49.375733	Acceso al sistema usuario admin
56	2019-08-18	12:46:15.603113	Acceso al sistema usuario admin
57	2019-08-18	12:48:51.063423	Acceso al sistema usuario admin
58	2019-08-18	12:52:20.924786	Acceso al sistema usuario admin
59	2019-08-18	13:54:56.07288	Acceso al sistema usuario admin
60	2019-08-25	11:25:59.036845	Acceso al sistema usuario admin
61	2019-08-25	11:29:37.187825	Acceso al sistema usuario admin
62	2019-08-25	15:10:30.983009	Acceso al sistema usuario admin
63	2019-08-25	15:15:10.801517	Acceso al sistema usuario admin
64	2019-08-25	15:16:21.3687	Acceso al sistema usuario admin
65	2019-08-25	15:17:11.067307	Acceso al sistema usuario admin
66	2019-08-25	15:18:07.334508	Acceso al sistema usuario admin
67	2019-08-25	15:19:47.516662	Acceso al sistema usuario admin
68	2019-08-25	16:07:16.95342	Acceso al sistema usuario admin
69	2019-08-25	16:32:22.2377	Acceso al sistema usuario admin
70	2019-08-25	16:41:38.599968	Acceso al sistema usuario admin
71	2019-08-25	16:44:49.040485	Acceso al sistema usuario admin
72	2019-08-25	16:51:05.596944	Acceso al sistema usuario admin
73	2019-08-25	17:20:08.863514	Acceso al sistema usuario admin
74	2019-09-01	21:37:54.230191	Acceso al sistema usuario admin
75	2019-09-01	21:40:19.096932	Acceso al sistema usuario admin
76	2019-09-01	21:45:14.279768	Acceso al sistema usuario admin
77	2019-09-04	20:52:40.648679	Acceso al sistema usuario admin
78	2019-09-08	08:58:56.900275	Acceso al sistema usuario admin
79	2019-09-08	09:00:27.851442	Acceso al sistema usuario admin
80	2019-09-08	10:05:01.010471	Acceso al sistema usuario admin
81	2019-09-08	10:57:05.102178	User, insertar Usuario rasg, perfil: 0
82	2019-09-08	11:04:40.147286	Acceso al sistema usuario admin
83	2019-09-08	11:05:14.345207	Inserción de parametro: admin, rastrahm
84	2019-09-08	11:14:10.685035	Cambio de parametro: 13, admin de valor rastrahm a admin
85	2019-09-08	11:15:48.514706	Cambio de parametro: 12, admin de valor  a rasg
86	2019-09-08	11:23:06.441258	Inserción de Usuario: prueba, 0
87	2019-09-08	11:23:06.441258	User, insertar Usuario prueba, perfil: 0
91	2019-09-08	12:21:12.444589	Inserción de articulo: prueba, prueba
92	2019-09-08	12:25:26.625183	Edición de articulo: 1 de: prueba a: prueba, de: prueba a: prueba 2
93	2019-09-18	21:10:52.852147	Acceso al sistema usuario admin
94	2019-09-18	21:24:55.003047	Acceso al sistema usuario admin
95	2019-09-21	20:42:21.033012	Acceso al sistema usuario admin
96	2019-09-22	10:33:38.55851	Acceso al sistema usuario admin
97	2019-10-06	09:06:23.562351	Acceso al sistema usuario admin
98	2019-10-06	09:09:27.673082	Acceso al sistema usuario admin
99	2019-10-06	09:10:45.652423	Acceso al sistema usuario admin
100	2019-10-06	09:23:55.782928	Acceso al sistema usuario admin
101	2019-10-06	09:40:06.461346	Acceso al sistema usuario admin
102	2019-10-06	09:42:00.489722	Acceso al sistema usuario admin
103	2019-10-06	09:45:13.776045	Acceso al sistema usuario admin
104	2019-10-06	09:58:11.061763	Acceso al sistema usuario admin
105	2019-10-06	10:02:03.212344	Acceso al sistema usuario admin
106	2019-10-06	10:09:10.962778	Acceso al sistema usuario admin
107	2019-10-06	10:16:30.804895	Acceso al sistema usuario admin
108	2019-10-06	10:26:03.642803	Acceso al sistema usuario admin
109	2019-10-06	10:29:16.375891	Acceso al sistema usuario admin
110	2019-10-06	10:30:19.861106	Acceso al sistema usuario admin
111	2019-10-06	10:31:06.28664	Acceso al sistema usuario admin
112	2019-10-06	10:31:14.137945	Acceso al sistema usuario admin
113	2019-10-06	10:38:57.05183	Acceso al sistema usuario admin
114	2019-10-06	10:39:16.956298	Acceso al sistema usuario admin
115	2019-10-06	10:41:07.992965	Acceso al sistema usuario admin
116	2019-10-06	10:41:23.343308	Acceso al sistema usuario admin
117	2019-10-06	10:41:56.907482	Acceso al sistema usuario admin
118	2019-10-06	10:43:06.09611	Acceso al sistema usuario admin
119	2019-10-06	10:53:38.472799	Acceso al sistema usuario admin
120	2019-10-06	10:59:23.598368	Acceso al sistema usuario admin
121	2019-10-06	11:00:01.311651	Acceso al sistema usuario admin
122	2019-10-06	11:00:07.545991	Acceso al sistema usuario admin
123	2019-10-06	11:02:53.962405	Acceso al sistema usuario admin
124	2019-10-06	11:16:00.47988	Acceso al sistema usuario admin
125	2019-10-06	11:25:20.993036	Acceso al sistema usuario admin
126	2019-10-06	11:29:17.296981	Acceso al sistema usuario admin
127	2019-10-06	11:32:26.826963	Acceso al sistema usuario admin
128	2019-10-13	10:08:46.296413	Acceso al sistema usuario admin
129	2019-10-13	10:09:11.07265	Acceso al sistema usuario admin
130	2019-10-13	11:25:17.494558	Acceso al sistema usuario admin
131	2019-10-13	11:30:50.074009	Acceso al sistema usuario admin
132	2019-10-13	11:31:14.520895	Acceso al sistema usuario admin
133	2019-10-13	11:36:48.614304	Acceso al sistema usuario admin
134	2019-10-13	11:43:31.730097	Acceso al sistema usuario admin
135	2019-10-13	11:56:30.253011	Acceso al sistema usuario admin
136	2019-10-13	12:00:22.839383	Acceso al sistema usuario admin
137	2019-10-13	12:03:30.406228	Acceso al sistema usuario admin
138	2019-10-13	12:20:01.031959	Acceso al sistema usuario admin
139	2019-10-13	12:47:21.421895	Acceso al sistema usuario admin
140	2019-10-13	12:53:25.002329	Acceso al sistema usuario admin
141	2019-10-13	12:59:29.49051	Acceso al sistema usuario admin
142	2019-10-13	13:04:56.312907	Acceso al sistema usuario admin
143	2019-10-13	19:32:08.940086	Acceso al sistema usuario admin
144	2019-10-13	19:38:08.391375	Acceso al sistema usuario admin
145	2019-10-13	19:46:56.576706	Acceso al sistema usuario admin
146	2019-10-13	19:50:07.835389	Acceso al sistema usuario admin
147	2019-10-13	19:52:46.424887	Acceso al sistema usuario admin
148	2019-10-13	19:56:50.150728	Acceso al sistema usuario admin
149	2019-10-13	20:15:55.746698	Acceso al sistema usuario admin
150	2019-10-13	20:39:46.865301	Acceso al sistema usuario admin
151	2019-10-13	20:41:29.79069	Acceso al sistema usuario admin
152	2019-10-13	20:43:29.897427	Acceso al sistema usuario admin
153	2019-10-13	20:46:30.311374	Acceso al sistema usuario admin
154	2019-10-14	20:37:44.31446	Acceso al sistema usuario admin
155	2019-10-14	20:43:53.184977	Acceso al sistema usuario admin
156	2019-10-14	20:50:46.383973	Acceso al sistema usuario admin
157	2019-10-14	20:52:43.690493	Acceso al sistema usuario admin
158	2019-10-14	20:53:46.999119	Acceso al sistema usuario admin
159	2019-10-14	21:10:54.853625	Acceso al sistema usuario admin
160	2019-10-14	21:13:47.008017	Acceso al sistema usuario admin
161	2019-10-14	21:16:26.884635	Acceso al sistema usuario admin
162	2019-10-14	21:17:26.551275	Acceso al sistema usuario admin
163	2019-10-14	21:20:43.199428	Acceso al sistema usuario admin
164	2019-10-15	21:03:55.819657	Acceso al sistema usuario admin
165	2019-10-15	21:06:02.254425	Acceso al sistema usuario admin
166	2019-10-15	21:06:50.574626	Acceso al sistema usuario admin
167	2019-10-15	21:17:50.313336	Acceso al sistema usuario admin
168	2019-10-15	21:18:51.278937	Acceso al sistema usuario admin
169	2019-10-15	21:20:30.307838	Acceso al sistema usuario admin
170	2019-10-15	21:22:18.730024	Acceso al sistema usuario admin
171	2019-10-15	21:24:44.249221	Acceso al sistema usuario admin
172	2019-10-15	21:25:55.305546	Acceso al sistema usuario admin
173	2019-10-15	21:29:12.080545	Acceso al sistema usuario admin
174	2019-10-15	21:34:02.861658	Acceso al sistema usuario admin
175	2019-10-15	21:35:52.361768	Acceso al sistema usuario admin
176	2019-10-15	21:38:25.360621	Acceso al sistema usuario admin
177	2019-10-20	09:42:09.589494	Acceso al sistema usuario admin
178	2019-10-20	09:43:14.390821	Acceso al sistema usuario admin
179	2019-10-20	09:44:12.826831	Acceso al sistema usuario admin
180	2019-10-20	10:02:15.843523	Acceso al sistema usuario admin
181	2019-10-20	10:03:48.079301	Acceso al sistema usuario admin
182	2019-10-20	10:11:49.036481	Acceso al sistema usuario admin
183	2019-10-20	10:34:57.511545	Acceso al sistema usuario admin
184	2019-10-20	11:33:20.417462	Acceso al sistema usuario admin
185	2019-10-20	11:39:48.95749	Acceso al sistema usuario admin
186	2019-10-20	12:08:30.664981	Acceso al sistema usuario admin
187	2019-10-20	12:11:23.745932	Acceso al sistema usuario admin
188	2019-10-20	13:15:01.919697	Acceso al sistema usuario admin
189	2019-10-20	13:39:34.781291	Acceso al sistema usuario admin
190	2019-10-20	13:57:49.286627	Acceso al sistema usuario admin
191	2019-10-20	14:08:21.717402	Acceso al sistema usuario admin
192	2019-10-20	14:19:53.888273	Acceso al sistema usuario admin
193	2019-10-20	14:23:31.004333	Acceso al sistema usuario admin
194	2019-10-20	14:42:22.109554	Acceso al sistema usuario admin
195	2019-10-20	14:55:44.342161	Acceso al sistema usuario admin
196	2019-10-20	15:02:20.620738	Acceso al sistema usuario admin
197	2019-10-20	15:39:28.708936	Acceso al sistema usuario admin
198	2019-10-20	15:48:52.190096	Acceso al sistema usuario admin
199	2019-10-20	15:55:18.077135	Acceso al sistema usuario admin
200	2019-10-20	16:04:56.768656	Acceso al sistema usuario admin
201	2019-10-20	16:06:56.935754	Acceso al sistema usuario admin
202	2019-10-20	16:09:29.788244	Acceso al sistema usuario admin
203	2019-10-20	16:13:43.849856	Acceso al sistema usuario admin
204	2019-10-20	16:20:55.605956	Acceso al sistema usuario admin
205	2019-10-20	16:26:49.090014	Acceso al sistema usuario admin
206	2019-10-20	16:33:53.908154	Acceso al sistema usuario admin
207	2019-10-20	16:55:00.324771	Acceso al sistema usuario admin
208	2019-10-20	16:58:53.695778	Acceso al sistema usuario admin
209	2019-10-20	17:07:19.161226	Acceso al sistema usuario admin
210	2019-10-20	17:12:08.484849	Acceso al sistema usuario admin
211	2019-10-20	17:28:08.343048	Acceso al sistema usuario admin
212	2019-10-20	17:32:02.849273	Acceso al sistema usuario admin
213	2019-10-20	17:38:38.368884	Acceso al sistema usuario admin
214	2019-10-20	17:41:02.614807	Acceso al sistema usuario admin
215	2019-10-20	17:46:35.028958	Acceso al sistema usuario admin
222	2019-10-27	10:07:51.629378	Acceso al sistema usuario admin
223	2019-10-27	10:08:07.556166	Acceso al sistema usuario admin
224	2019-10-27	10:09:39.968275	Acceso al sistema usuario admin
225	2019-10-27	10:15:34.858726	Acceso al sistema usuario admin
226	2019-10-27	10:25:12.683441	Acceso al sistema usuario admin
227	2019-10-27	10:27:13.473079	Acceso al sistema usuario admin
228	2019-10-27	10:43:33.244339	Acceso al sistema usuario admin
229	2019-10-27	10:55:34.384422	Acceso al sistema usuario admin
230	2019-10-27	11:44:00.729387	Acceso al sistema usuario admin
231	2019-10-27	11:48:54.774606	Acceso al sistema usuario admin
232	2019-10-27	12:00:47.570105	Acceso al sistema usuario admin
233	2019-10-27	12:11:11.776257	Acceso al sistema usuario admin
234	2019-10-27	12:42:19.520018	Acceso al sistema usuario admin
235	2019-10-27	13:51:47.137218	Acceso al sistema usuario admin
236	2019-10-27	14:03:27.57145	Acceso al sistema usuario admin
237	2019-10-27	14:09:13.405307	Acceso al sistema usuario admin
238	2019-10-27	18:29:17.078523	Acceso al sistema usuario admin
239	2019-10-27	18:39:42.706891	Acceso al sistema usuario admin
240	2019-10-27	18:48:49.534313	Acceso al sistema usuario admin
241	2019-10-27	19:02:31.176258	Acceso al sistema usuario admin
242	2019-10-27	20:09:41.652747	Acceso al sistema usuario admin
243	2019-10-27	20:23:56.313283	Acceso al sistema usuario admin
244	2019-10-27	20:31:20.207241	Acceso al sistema usuario admin
245	2019-10-27	20:34:25.297115	Acceso al sistema usuario admin
246	2019-10-27	20:35:40.730228	Acceso al sistema usuario admin
247	2019-10-27	20:39:52.542458	Acceso al sistema usuario admin
248	2019-10-28	20:59:49.070742	Acceso al sistema usuario admin
249	2019-10-28	21:01:16.602606	Acceso al sistema usuario admin
250	2019-10-28	21:07:59.906033	Acceso al sistema usuario admin
251	2019-10-28	21:11:13.660045	Acceso al sistema usuario admin
252	2019-10-28	21:16:05.252314	Acceso al sistema usuario admin
253	2019-10-28	21:21:18.838214	Acceso al sistema usuario admin
254	2019-10-28	21:23:03.178836	Acceso al sistema usuario admin
255	2019-10-28	21:36:00.715435	Acceso al sistema usuario admin
256	2019-10-28	21:48:25.316762	Acceso al sistema usuario admin
257	2019-10-28	21:49:34.354263	Acceso al sistema usuario admin
258	2019-10-28	21:51:28.666865	Acceso al sistema usuario admin
259	2019-10-30	21:27:14.16112	Acceso al sistema usuario admin
260	2019-10-30	21:43:56.47612	Acceso al sistema usuario admin
261	2019-10-30	21:44:00.56818	Acceso al sistema usuario admin
262	2019-10-30	21:47:57.998454	Acceso al sistema usuario admin
263	2019-10-30	21:51:01.316522	Acceso al sistema usuario admin
264	2019-10-30	21:56:02.207922	Acceso al sistema usuario admin
265	2019-10-30	22:01:12.725814	Acceso al sistema usuario admin
266	2019-11-03	09:05:08.518839	Acceso al sistema usuario admin
267	2019-11-03	09:08:40.589194	Acceso al sistema usuario admin
268	2019-11-03	09:13:58.850034	Acceso al sistema usuario admin
269	2019-11-03	09:49:48.060795	Inserción de parametro: prueba, valor
270	2019-11-03	09:58:40.525137	Acceso al sistema usuario admin
271	2019-11-03	10:00:06.388623	Acceso al sistema usuario admin
272	2019-11-03	10:04:25.862737	Acceso al sistema usuario admin
273	2019-11-03	10:11:19.242799	Inserción de parametro: prueba 1, valor 1
274	2019-11-03	10:13:09.006742	Inserción de parametro: prueba 2, valor 2
275	2019-11-03	10:20:13.734857	Acceso al sistema usuario admin
276	2019-11-03	10:23:33.363298	Acceso al sistema usuario admin
277	2019-11-03	10:51:28.013127	Acceso al sistema usuario admin
278	2019-11-03	11:49:04.580882	Acceso al sistema usuario admin
279	2019-11-03	11:53:03.0516	Acceso al sistema usuario admin
280	2019-11-03	11:57:19.198819	Acceso al sistema usuario admin
281	2019-11-03	12:08:13.379708	Acceso al sistema usuario admin
282	2019-11-03	12:19:09.703154	Acceso al sistema usuario admin
283	2019-11-03	12:23:17.506647	Inserción de Usuario: prueba 1, 0
284	2019-11-03	12:23:17.506647	User, insertar Usuario prueba 1, perfil: 0
285	2019-11-03	12:24:31.621351	Acceso al sistema usuario admin
286	2019-11-03	12:25:24.780405	Acceso al sistema usuario admin
287	2019-11-03	12:28:41.033139	Inserción de Usuario: prueba 2, 0
288	2019-11-03	12:28:41.033139	User, insertar Usuario prueba 2, perfil: 0
289	2019-11-03	12:54:21.183441	Acceso al sistema usuario admin
290	2019-11-03	14:45:06.475517	Acceso al sistema usuario admin
293	2019-11-03	14:51:22.943592	Inserción de Usuario: prueba 3, 0
294	2019-11-03	14:51:22.943592	User, insertar Usuario prueba 3, perfil: 0
295	2019-11-03	15:06:54.26011	Acceso al sistema usuario admin
296	2019-11-03	15:09:29.745047	Inserción de parametro: prueba 3, valor 3
297	2019-11-03	15:10:04.924618	Acceso al sistema usuario admin
298	2019-11-03	15:11:39.158458	Acceso al sistema usuario admin
299	2019-11-03	15:20:59.663539	Acceso al sistema usuario admin
300	2019-11-03	15:22:33.379062	Acceso al sistema usuario admin
301	2019-11-03	15:24:01.603467	Inserción de Usuario: prueba 5, 0
302	2019-11-03	15:24:01.603467	User, insertar Usuario prueba 5, perfil: 0
303	2019-11-03	15:31:51.360362	Acceso al sistema usuario admin
304	2019-11-03	15:40:32.207347	Acceso al sistema usuario admin
305	2019-11-03	15:43:28.467133	Acceso al sistema usuario admin
306	2019-11-03	15:44:57.787063	Acceso al sistema usuario admin
307	2019-11-03	19:01:11.628376	Acceso al sistema usuario admin
309	2019-11-03	19:10:00.132237	Inserción de articulo: Prueba, &lt;h1&gt;Prueba&lt;/h1&gt;, &lt;p&gt;Prueba de Articulos&lt;/p&gt;
310	2019-11-03	19:12:55.130032	Acceso al sistema usuario admin
311	2019-11-03	19:16:12.191523	Acceso al sistema usuario admin
312	2019-11-04	20:58:06.668579	Acceso al sistema usuario admin
313	2019-11-04	20:58:10.502832	Acceso al sistema usuario admin
314	2019-11-10	10:14:22.765407	Acceso al sistema usuario admin
315	2019-11-10	10:25:02.92884	Acceso al sistema usuario admin
316	2019-11-10	10:37:53.795198	Acceso al sistema usuario admin
317	2019-11-10	10:43:27.810188	Acceso al sistema usuario admin
318	2019-11-10	14:30:02.65895	Acceso al sistema usuario admin
319	2019-11-10	14:46:15.353784	Inserción de articulo: Articulo, Articulo, Articulo
320	2019-11-10	14:48:15.304382	Acceso al sistema usuario admin
321	2019-11-10	14:55:06.5532	Acceso al sistema usuario admin
322	2019-11-10	14:57:21.053147	Inserción de articulo: 4 Prueba, Prueba 4, Prueba 4
323	2019-11-10	15:00:36.166515	Acceso al sistema usuario admin
324	2019-11-11	13:13:53.423277	Acceso al sistema usuario admin
325	2019-11-11	13:21:32.856419	Acceso al sistema usuario admin
326	2019-11-11	13:22:50.699406	Inserción de articulo: Debugger, Debugger, Debugger
327	2019-11-11	13:26:58.591564	Acceso al sistema usuario admin
328	2019-11-11	13:28:59.067859	Inserción de articulo: Depurar, Depurar, Depuración del sistema
329	2019-11-11	13:54:21.621773	Acceso al sistema usuario admin
330	2019-11-11	13:56:06.555154	Acceso al sistema usuario admin
331	2019-11-11	13:57:21.286458	Inserción de articulo: event.preventDefault();, event.preventDefault();, event.preventDefault();
332	2019-11-11	14:09:06.210111	Acceso al sistema usuario admin
333	2019-11-11	14:10:37.768549	Inserción de articulo: Portada, Portada, Portada Articulo
334	2019-11-11	14:14:58.629947	Acceso al sistema usuario admin
335	2019-11-11	14:17:19.041741	Inserción de articulo: Articulo, Articulo, Articulo de prueba, sin referencia real
336	2019-11-11	15:08:21.028975	Acceso al sistema usuario admin
337	2019-11-11	16:01:03.547495	Edición de articulo: 2 de: Codigo a: Codigo; <h1>Codigo</h1> a: <h1>Codigo</h1>, de: Prueba a: <h3>Prueba</h3>\n<p>Texto de prueba</p>
338	2019-11-11	16:04:22.816189	Inserción de parametro: Prueba 4, Valor 4
339	2019-11-11	16:06:06.075186	Cambio de parametro: 17, Prueba 3 de valor valor 3 a Valor 3
340	2019-11-11	16:19:21.230967	Acceso al sistema usuario admin
341	2019-11-11	16:24:45.535605	Acceso al sistema usuario admin
342	2019-11-11	16:28:41.205279	Acceso al sistema usuario admin
343	2019-11-11	16:32:48.665313	Acceso al sistema usuario admin
344	2019-11-11	16:44:43.375498	Acceso al sistema usuario admin
345	2019-11-11	16:45:31.171698	Acceso al sistema usuario admin
346	2019-11-11	16:48:12.382922	Edición de Usuario: 3, prueba, 1, 0
347	2019-11-11	16:48:12.382922	User, actualización Usuario: prueba, Activo: 1, Perfil: 0
348	2019-11-11	16:52:41.291616	Edición de articulo: 2 de: Codigo a: Codigo; <h1>Codigo</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Prueba</h3>\n<p>Texto de prueba</p> a: <h3>Construcción del Código</h3>\n<p>Se trata de simplificar el código para descargar peso sobre el navegador y lograr una mayor velocidad de ejecución</p>
349	2019-11-11	16:53:42.513902	Cambio de parametro: 16, Prueba 2 de valor valor 2 a Valor 2
350	2019-11-11	17:37:09.729617	Acceso al sistema usuario admin
351	2019-11-11	17:39:47.010278	Acceso al sistema usuario admin
352	2019-11-11	17:42:05.098962	Acceso al sistema usuario admin
353	2019-11-11	17:55:29.295131	Acceso al sistema usuario admin
354	2019-11-11	17:59:11.860093	Acceso al sistema usuario admin
355	2019-11-12	13:30:06.172277	Acceso al sistema usuario admin
356	2019-11-12	14:13:37.267032	Eliminación de parametro: 18, 17, 18, 17
357	2019-11-12	14:14:18.852933	Acceso al sistema usuario admin
358	2019-11-12	14:15:04.819073	Eliminación de parametro: 15,16, 15,16
359	2019-11-12	14:18:49.561982	Eliminación de articulo: 10
361	2019-11-12	14:26:57.482752	Eliminación de Usuario: 4,5,6,7,3
362	2019-11-12	14:28:26.491761	Acceso al sistema usuario admin
363	2019-11-12	14:30:38.866756	Acceso al sistema usuario admin
364	2019-11-12	14:50:43.268416	Acceso al sistema usuario admin
365	2019-11-12	14:51:59.609157	Inserción de Usuario: prueba, 0
366	2019-11-12	14:51:59.609157	User, insertar Usuario prueba, perfil: 0
367	2019-11-12	14:59:41.443555	Edición de Usuario: 2, rasg, 1, 0
368	2019-11-12	14:59:41.443555	User, actualización Usuario: rasg, Activo: 1, Perfil: 0
369	2019-11-12	15:00:20.294611	Inserción de Usuario: prueba 1, 0
370	2019-11-12	15:00:20.294611	User, insertar Usuario prueba 1, perfil: 0
373	2019-11-12	15:08:45.591056	Eliminación de Usuario: 8,9
374	2019-11-12	15:26:32.877496	Acceso al sistema usuario admin
375	2019-11-12	15:28:32.271683	Acceso al sistema usuario admin
376	2019-11-12	15:33:40.222604	Acceso al sistema usuario admin
377	2019-11-12	15:36:41.772048	Acceso al sistema usuario admin
378	2019-11-12	15:48:25.009743	Acceso al sistema usuario admin
379	2019-11-12	15:57:21.138633	Acceso al sistema usuario admin
380	2019-11-12	15:59:36.618698	Acceso al sistema usuario admin
381	2019-11-12	16:01:23.937274	Acceso al sistema usuario admin
382	2019-11-12	16:09:20.494491	Acceso al sistema usuario admin
383	2019-11-17	13:42:55.079846	Acceso al sistema usuario admin
384	2019-11-17	13:54:04.747386	Inserción de parametro: Context, all
385	2019-11-17	14:02:34.452248	Cambio de parametro: 19, Context de valor all a all
386	2019-11-17	14:03:50.506617	Eliminación de parametro: 19, 19
477	2020-01-19	09:46:47.131945	Acceso al sistema usuario admin
387	2019-11-17	15:04:17.012555	Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>\n<p>Se trata de simplificar el código para descargar peso sobre el navegador y lograr una mayor velocidad de ejecución</p> a: <h3>Construcción del Código</h3>\n<p>Se trata de simplificar el código para descargar peso sobre el navegador y lograr una mayor velocidad de ejecución</p>
388	2019-11-18	14:33:50.654934	Acceso al sistema usuario admin
389	2019-11-18	16:28:01.527632	Acceso al sistema usuario admin
390	2019-11-18	17:14:00.001194	Acceso al sistema usuario admin
391	2019-11-18	17:22:03.683908	Acceso al sistema usuario admin
392	2019-11-18	17:36:43.23662	Acceso al sistema usuario admin
393	2019-11-18	17:49:05.93348	Acceso al sistema usuario admin
394	2019-11-18	18:03:08.761249	Acceso al sistema usuario admin
395	2019-11-18	18:03:53.674595	Acceso al sistema usuario admin
396	2019-11-18	18:07:09.261383	Acceso al sistema usuario admin
397	2019-11-18	18:20:27.797477	Acceso al sistema usuario admin
398	2019-11-24	08:44:29.747645	Acceso al sistema usuario admin
399	2019-11-24	08:51:23.206163	Acceso al sistema usuario admin
400	2019-11-24	10:00:41.961175	Acceso al sistema usuario admin
401	2019-11-24	10:29:48.991514	Acceso al sistema usuario admin
402	2019-11-24	11:32:26.565952	Acceso al sistema usuario admin
403	2019-11-24	11:33:53.618852	Acceso al sistema usuario admin
404	2019-11-24	11:45:34.985307	Acceso al sistema usuario admin
405	2019-11-24	11:53:06.667632	Acceso al sistema usuario admin
406	2019-11-24	11:59:00.934817	Acceso al sistema usuario admin
407	2019-11-24	12:38:37.216594	Acceso al sistema usuario admin
408	2019-11-24	19:16:58.673311	Acceso al sistema usuario admin
409	2019-12-22	10:07:01.941716	Acceso al sistema usuario admin
410	2019-12-22	10:08:49.524009	Acceso al sistema usuario admin
411	2019-12-22	10:09:39.352909	Acceso al sistema usuario admin
412	2019-12-22	15:00:08.137386	Acceso al sistema usuario admin
413	2019-12-22	15:02:39.771437	Acceso al sistema usuario admin
414	2019-12-22	15:02:59.529154	Acceso al sistema usuario admin
415	2019-12-22	18:42:30.962031	Acceso al sistema usuario admin
416	2019-12-22	18:43:20.134282	Acceso al sistema usuario admin
417	2019-12-24	11:22:20.680654	Acceso al sistema usuario admin
418	2019-12-24	11:22:22.378252	Acceso al sistema usuario admin
419	2019-12-24	11:22:55.921256	Acceso al sistema usuario admin
420	2019-12-29	09:09:28.839584	Acceso al sistema usuario admin
421	2019-12-29	09:23:33.555216	Acceso al sistema usuario admin
422	2019-12-29	10:09:53.340912	Acceso al sistema usuario admin
423	2019-12-29	10:27:32.997413	Acceso al sistema usuario admin
424	2019-12-29	10:35:58.891863	Acceso al sistema usuario admin
425	2019-12-29	11:05:18.999082	Acceso al sistema usuario admin
426	2019-12-29	11:15:08.338153	Acceso al sistema usuario admin
427	2019-12-29	11:43:19.051115	Acceso al sistema usuario admin
428	2019-12-29	12:01:00.950353	Acceso al sistema usuario admin
429	2019-12-29	12:07:32.289869	Acceso al sistema usuario admin
430	2019-12-29	12:19:02.773928	Acceso al sistema usuario admin
431	2019-12-29	12:28:27.070278	Acceso al sistema usuario admin
432	2019-12-29	13:17:59.19576	Acceso al sistema usuario admin
433	2019-12-29	13:20:41.509343	Acceso al sistema usuario admin
434	2019-12-29	13:23:16.277269	Acceso al sistema usuario admin
435	2019-12-29	13:28:31.058871	Acceso al sistema usuario admin
436	2019-12-29	13:30:46.578559	Acceso al sistema usuario admin
437	2019-12-29	13:33:02.317295	Acceso al sistema usuario admin
438	2019-12-29	13:44:33.903997	Acceso al sistema usuario admin
439	2019-12-29	13:50:07.004124	Acceso al sistema usuario admin
440	2020-01-05	12:27:32.509862	Acceso al sistema usuario admin
441	2020-01-05	12:32:22.036258	Acceso al sistema usuario admin
442	2020-01-05	12:53:19.344283	Acceso al sistema usuario admin
443	2020-01-05	12:54:21.994449	Acceso al sistema usuario admin
444	2020-01-05	12:56:59.971123	Acceso al sistema usuario admin
445	2020-01-05	13:00:58.690196	Acceso al sistema usuario admin
446	2020-01-05	13:03:16.812298	Acceso al sistema usuario admin
447	2020-01-05	13:05:19.904637	Acceso al sistema usuario admin
448	2020-01-12	12:24:58.325159	Acceso al sistema usuario admin
449	2020-01-12	12:28:05.192342	Acceso al sistema usuario admin
450	2020-01-12	12:29:25.540432	Cambio de parametro: 1, LOGO de valor  a /var/pruebaphp
451	2020-01-12	12:31:46.919375	Cambio de parametro: 1, LOGO de valor /var/pruebaphp a /var/prueba
452	2020-01-12	12:37:50.548289	Acceso al sistema usuario admin
453	2020-01-12	12:50:26.896972	Acceso al sistema usuario admin
454	2020-01-12	12:55:10.407057	Acceso al sistema usuario admin
455	2020-01-12	13:23:38.090524	Acceso al sistema usuario admin
456	2020-01-12	13:29:22.371037	Acceso al sistema usuario admin
457	2020-01-12	13:51:03.720878	Acceso al sistema usuario admin
458	2020-01-12	17:26:27.315266	Acceso al sistema usuario admin
459	2020-01-12	17:32:37.477666	Acceso al sistema usuario admin
460	2020-01-12	17:46:31.523176	Acceso al sistema usuario admin
461	2020-01-12	18:08:59.717802	Acceso al sistema usuario admin
462	2020-01-12	18:33:22.270403	Cambio de parametro: 1, LOGO de valor /var/prueba a /var/pruebaJava
463	2020-01-19	07:40:20.579981	Acceso al sistema usuario admin
464	2020-01-19	07:40:53.852284	Acceso al sistema usuario admin
465	2020-01-19	07:41:52.150964	Acceso al sistema usuario admin
466	2020-01-19	07:59:01.507288	Acceso al sistema usuario admin
467	2020-01-19	08:02:04.788921	Acceso al sistema usuario admin
468	2020-01-19	08:29:30.909782	Acceso al sistema usuario admin
469	2020-01-19	08:34:07.388301	Acceso al sistema usuario admin
470	2020-01-19	08:38:33.536091	Inserción de parametro: prueba, Prueba Java
471	2020-01-19	08:42:42.33634	Inserción de articulo: Prueba, <h1>Prueba</h1>, <p>Prueba</p>
472	2020-01-19	08:52:02.212096	Acceso al sistema usuario admin
473	2020-01-19	09:33:40.876633	Acceso al sistema usuario admin
474	2020-01-19	09:34:41.579363	Acceso al sistema usuario admin
475	2020-01-19	09:36:16.616666	Acceso al sistema usuario admin
476	2020-01-19	09:38:21.660748	Acceso al sistema usuario admin
478	2020-01-19	10:03:01.887149	Acceso al sistema usuario admin
479	2020-01-19	10:25:01.540686	Inserción de Usuario: prueba, 0
480	2020-01-19	10:25:01.540686	User, insertar Usuario prueba, perfil: 0
481	2020-01-19	10:32:12.504074	Acceso al sistema usuario admin
482	2020-01-19	10:42:37.565603	Eliminación de parametro: 20, 20
483	2020-01-23	07:32:09.46888	Acceso al sistema usuario admin
484	2020-02-06	11:16:10.079298	Acceso al sistema usuario admin
485	2020-02-06	14:12:31.273638	Acceso al sistema usuario admin
486	2020-02-06	14:26:25.551406	Acceso al sistema usuario admin
487	2020-02-06	14:27:14.206852	Acceso al sistema usuario admin
488	2020-02-06	14:27:56.54714	Acceso al sistema usuario admin
489	2020-02-06	14:29:24.950595	Acceso al sistema usuario admin
490	2020-02-06	14:31:44.276541	Acceso al sistema usuario admin
491	2020-02-06	14:32:43.520124	Acceso al sistema usuario admin
492	2020-02-06	14:34:53.102094	Acceso al sistema usuario admin
493	2020-02-06	14:50:23.775051	Acceso al sistema usuario admin
494	2020-02-06	14:51:16.558231	Acceso al sistema usuario admin
495	2020-02-06	14:52:01.093683	Acceso al sistema usuario admin
496	2020-02-06	14:52:13.113762	Acceso al sistema usuario admin
497	2020-02-06	14:55:44.055748	Acceso al sistema usuario admin
498	2020-02-06	14:58:04.746681	Acceso al sistema usuario admin
499	2020-02-06	14:59:55.238666	Acceso al sistema usuario admin
500	2020-02-06	15:25:30.143368	Acceso al sistema usuario admin
501	2020-02-06	15:27:20.009873	Acceso al sistema usuario admin
502	2020-02-06	15:28:49.457466	Acceso al sistema usuario admin
503	2020-02-06	15:29:24.778224	Acceso al sistema usuario admin
504	2020-02-06	15:29:44.087692	Acceso al sistema usuario admin
505	2020-02-06	17:08:01.211308	Acceso al sistema usuario admin
506	2020-02-07	08:27:49.846779	Acceso al sistema usuario admin
507	2020-02-07	08:38:25.052939	Acceso al sistema usuario admin
508	2020-02-07	09:16:33.609774	Acceso al sistema usuario admin
509	2020-02-07	09:46:39.649713	Acceso al sistema usuario admin
510	2020-02-07	09:47:53.077487	Acceso al sistema usuario admin
511	2020-02-07	11:08:48.09258	Acceso al sistema usuario admin
512	2020-02-07	11:26:11.347111	Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>\n<p>Se trata de simplificar el código para descargar peso sobre el navegador y lograr una mayor velocidad de ejecución</p> a: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>
513	2020-02-07	12:08:22.086239	Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>
514	2020-02-07	12:09:05.751941	Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>
515	2020-02-07	12:09:22.703103	Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>
516	2020-02-07	12:10:11.853959	Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>
517	2020-02-07	12:13:35.907766	Acceso al sistema usuario admin
518	2020-02-07	14:46:07.412912	Acceso al sistema usuario admin
519	2020-02-07	14:47:01.430061	Inserción de parametro: prueba_3, prueba 3
520	2020-02-07	14:55:17.344934	Inserción de parametro: prueba_4, prueba 4
521	2020-02-07	15:00:05.766911	Eliminación de parametro: 21, 21
522	2020-02-07	15:06:00.914888	Eliminación de parametro: 22, 22
523	2020-02-07	15:26:20.33566	Acceso al sistema usuario undefined
524	2020-02-07	15:31:04.977339	Acceso al sistema usuario undefined
525	2020-02-07	15:32:36.478909	Acceso al sistema usuario admin
526	2020-02-07	15:33:02.720483	Acceso al sistema usuario admin
527	2020-02-07	15:34:04.441954	Acceso al sistema usuario undefined
528	2020-02-07	15:36:17.596287	Acceso al sistema usuario undefined
529	2020-02-08	09:24:49.632685	Acceso al sistema usuario undefined
530	2020-02-08	09:25:42.075014	Acceso al sistema usuario undefined
531	2020-02-08	09:32:54.668208	Acceso al sistema usuario undefined
532	2020-02-08	09:43:23.110895	Acceso al sistema usuario undefined
533	2020-02-08	09:43:23.120722	Acceso al sistema usuario undefined
534	2020-02-08	10:09:43.020299	Acceso al sistema usuario undefined
535	2020-02-08	10:10:47.072164	Acceso al sistema usuario undefined
536	2020-02-08	10:26:10.007522	Acceso al sistema usuario undefined
537	2020-02-08	10:34:06.4833	Acceso al sistema usuario admin
538	2020-02-08	10:35:40.843982	Acceso al sistema usuario undefined
539	2020-02-08	10:36:04.622378	Acceso al sistema usuario admin
540	2020-02-09	08:17:30.32148	Acceso al sistema usuario admin
541	2020-02-09	08:36:10.433407	Acceso al sistema usuario admin
542	2020-02-09	08:38:38.038344	Acceso al sistema usuario admin
543	2020-02-16	09:08:25.158365	Acceso al sistema usuario admin
544	2020-02-16	09:16:44.607697	Acceso al sistema usuario admin
545	2020-02-16	09:20:17.274551	Acceso al sistema usuario admin
546	2020-02-16	09:23:11.507769	Acceso al sistema usuario 
547	2020-02-16	09:30:14.856394	Acceso al sistema usuario 
548	2020-02-16	09:33:19.260973	Acceso al sistema usuario admin
549	2020-02-16	09:41:18.362821	Acceso al sistema usuario admin
550	2020-02-16	09:47:05.726543	Acceso al sistema usuario admin
551	2020-02-16	09:47:49.129702	Acceso al sistema usuario admin
552	2020-02-16	09:49:12.477972	Acceso al sistema usuario admin
553	2020-02-16	09:50:36.294231	Acceso al sistema usuario admin
554	2020-02-16	14:02:29.199159	Acceso al sistema usuario admin
555	2020-02-16	14:17:53.29672	Acceso al sistema usuario admin
556	2020-02-16	18:34:58.95497	Acceso al sistema usuario admin
557	2020-02-16	18:36:51.320756	Acceso al sistema usuario admin
558	2020-02-16	18:37:25.588602	Acceso al sistema usuario admin
559	2020-02-16	18:38:06.843222	Acceso al sistema usuario admin
560	2020-02-16	18:39:28.452796	Acceso al sistema usuario admin
561	2020-02-16	18:40:20.948714	Acceso al sistema usuario admin
562	2020-02-16	18:46:06.221971	Acceso al sistema usuario admin
563	2020-02-16	18:46:47.724802	Acceso al sistema usuario admin
564	2020-02-16	18:47:20.088113	Acceso al sistema usuario admin
565	2020-02-16	18:48:53.995197	Acceso al sistema usuario admin
566	2020-02-16	18:50:39.974041	Acceso al sistema usuario admin
567	2020-02-16	18:52:43.788151	Acceso al sistema usuario admin
568	2020-02-16	18:53:47.047734	Acceso al sistema usuario admin
569	2020-02-16	19:03:34.638551	Acceso al sistema usuario admin
570	2020-02-16	19:05:42.311924	Acceso al sistema usuario admin
571	2020-02-16	19:06:10.455997	Acceso al sistema usuario admin
572	2020-02-16	19:07:52.969079	Acceso al sistema usuario admin
573	2020-02-16	19:09:05.547557	Acceso al sistema usuario admin
574	2020-02-16	19:10:19.178842	Acceso al sistema usuario admin
575	2020-02-16	19:13:21.838056	Acceso al sistema usuario admin
576	2020-02-16	19:13:50.617386	Acceso al sistema usuario admin
577	2020-03-27	10:32:56.969374	Acceso al sistema usuario admin
578	2020-03-27	10:53:11.13048	Acceso al sistema usuario admin
579	2020-03-27	11:25:33.713188	Acceso al sistema usuario admin
580	2020-04-01	14:01:12.941399	Acceso al sistema usuario admin
581	2020-04-01	14:02:04.839015	Acceso al sistema usuario admin
582	2020-04-01	14:03:30.384659	Acceso al sistema usuario admin
583	2020-04-01	14:05:59.702647	Acceso al sistema usuario admin
584	2020-04-01	15:04:59.525378	Acceso al sistema usuario admin
585	2020-04-01	15:09:49.753539	Acceso al sistema usuario admin
586	2020-04-01	15:14:40.476156	Acceso al sistema usuario admin
587	2020-04-01	15:42:45.012137	Acceso al sistema usuario admin
588	2020-04-01	15:43:29.608389	Acceso al sistema usuario admin
589	2020-04-01	15:59:33.829848	Acceso al sistema usuario admin
590	2020-04-01	16:12:42.840926	Acceso al sistema usuario admin
591	2020-04-01	16:15:59.76045	Acceso al sistema usuario admin
592	2020-04-01	16:22:32.206405	Acceso al sistema usuario admin
593	2020-04-01	16:50:59.052591	Acceso al sistema usuario admin
594	2020-04-01	16:52:54.456572	Acceso al sistema usuario admin
595	2020-04-02	09:05:37.131575	Acceso al sistema usuario admin
596	2020-04-02	10:02:01.760932	Acceso al sistema usuario admin
597	2020-04-02	10:05:32.571669	Acceso al sistema usuario admin
598	2020-04-02	18:11:49.48154	Acceso al sistema usuario admin
599	2020-04-02	18:12:49.13926	Acceso al sistema usuario admin
600	2020-04-02	18:14:18.848048	Acceso al sistema usuario admin
601	2020-04-02	18:15:23.216207	Acceso al sistema usuario admin
602	2020-04-02	18:22:33.577977	Acceso al sistema usuario admin
603	2020-04-02	18:23:40.752857	Acceso al sistema usuario admin
604	2020-04-02	18:29:49.85689	Acceso al sistema usuario admin
605	2020-04-02	18:30:21.801798	Acceso al sistema usuario admin
606	2020-04-02	18:31:45.913854	Acceso al sistema usuario admin
607	2020-04-05	17:38:55.407926	Acceso al sistema usuario admin
608	2020-04-05	17:42:35.604928	Acceso al sistema usuario admin
609	2020-04-05	17:44:34.672582	Acceso al sistema usuario admin
610	2020-04-05	17:46:55.60132	Acceso al sistema usuario admin
611	2020-04-05	17:56:42.003362	Acceso al sistema usuario admin
612	2020-04-05	17:58:24.814519	Acceso al sistema usuario admin
613	2020-04-05	17:58:57.933383	Acceso al sistema usuario admin
614	2020-04-05	18:01:53.298887	Acceso al sistema usuario admin
615	2020-04-05	18:11:36.804794	Acceso al sistema usuario admin
616	2020-04-05	18:12:49.621756	Acceso al sistema usuario admin
617	2020-04-05	18:15:10.557614	Acceso al sistema usuario admin
618	2020-04-05	18:16:31.903694	Acceso al sistema usuario admin
619	2020-04-05	18:19:41.176282	Acceso al sistema usuario admin
620	2020-04-05	18:22:04.240874	Acceso al sistema usuario admin
621	2020-04-05	18:22:43.429917	Acceso al sistema usuario admin
622	2020-04-05	18:23:01.52164	Acceso al sistema usuario admin
623	2020-04-05	18:23:25.971495	Acceso al sistema usuario admin
624	2020-04-05	18:25:26.783525	Acceso al sistema usuario admin
625	2020-04-05	18:26:01.264095	Acceso al sistema usuario admin
626	2020-04-05	18:27:02.506447	Acceso al sistema usuario admin
627	2020-04-05	18:27:41.558172	Acceso al sistema usuario admin
628	2020-04-05	18:28:02.408822	Acceso al sistema usuario admin
629	2020-04-05	18:28:27.538887	Acceso al sistema usuario admin
630	2020-04-05	18:28:56.686165	Acceso al sistema usuario admin
631	2020-04-05	18:29:41.592637	Acceso al sistema usuario admin
632	2020-04-05	18:32:05.773618	Acceso al sistema usuario admin
633	2020-04-05	18:33:09.281142	Acceso al sistema usuario admin
634	2020-04-05	18:33:57.694754	Acceso al sistema usuario admin
635	2020-04-05	18:34:57.510065	Acceso al sistema usuario admin
636	2020-04-08	08:28:45.079674	Acceso al sistema usuario admin
637	2020-04-08	08:50:28.551286	Acceso al sistema usuario admin
638	2020-04-08	09:20:18.964003	Acceso al sistema usuario admin
639	2020-04-08	09:22:18.058365	Acceso al sistema usuario admin
640	2020-04-08	09:26:57.959752	Acceso al sistema usuario admin
641	2020-04-08	09:29:25.512161	Acceso al sistema usuario admin
642	2020-04-08	10:40:53.375192	Acceso al sistema usuario admin
643	2020-04-08	14:38:07.559846	Acceso al sistema usuario admin
644	2020-04-08	14:39:32.659654	Acceso al sistema usuario admin
645	2020-04-08	14:41:53.544758	Acceso al sistema usuario admin
646	2020-04-08	14:43:57.393093	Acceso al sistema usuario admin
647	2020-04-08	15:08:19.498972	Acceso al sistema usuario admin
648	2020-04-08	16:17:02.405965	Acceso al sistema usuario admin
649	2020-04-08	16:19:51.805139	Acceso al sistema usuario admin
650	2020-04-08	16:41:27.829468	Acceso al sistema usuario admin
651	2020-04-08	16:43:21.512258	Acceso al sistema usuario admin
652	2020-04-08	16:44:02.966336	Acceso al sistema usuario admin
653	2020-04-08	16:44:58.629627	Acceso al sistema usuario admin
654	2020-04-10	15:16:16.040804	Acceso al sistema usuario admin
655	2020-04-10	15:18:07.32176	Acceso al sistema usuario admin
656	2020-04-10	15:19:03.566912	Acceso al sistema usuario admin
657	2020-04-10	15:23:02.975426	Acceso al sistema usuario admin
658	2020-04-10	15:29:44.933304	Acceso al sistema usuario admin
659	2020-04-10	15:32:00.244568	Acceso al sistema usuario admin
660	2020-04-10	15:33:55.985844	Acceso al sistema usuario admin
661	2020-04-10	15:36:56.207062	Acceso al sistema usuario admin
662	2020-04-10	15:38:21.780005	Acceso al sistema usuario adimin
663	2020-04-10	15:39:09.194469	Acceso al sistema usuario admin
664	2020-04-10	15:40:04.555355	Acceso al sistema usuario admin
665	2020-04-10	15:41:31.313252	Acceso al sistema usuario admin
666	2020-04-10	15:43:16.05078	Acceso al sistema usuario admin
667	2020-04-10	15:43:49.247227	Acceso al sistema usuario admin
668	2020-04-10	15:44:20.641105	Acceso al sistema usuario admin
669	2020-04-10	15:46:06.022679	Acceso al sistema usuario admin
670	2020-04-10	16:08:25.166057	Acceso al sistema usuario admin
671	2020-04-12	14:32:52.439823	Acceso al sistema usuario admin
672	2020-04-12	14:42:58.23612	Acceso al sistema usuario admin
673	2020-04-12	17:20:03.560689	Acceso al sistema usuario admin
674	2020-04-12	17:24:10.697331	Acceso al sistema usuario admin
675	2020-04-12	17:34:50.049249	Acceso al sistema usuario admin
676	2020-04-12	17:37:02.389335	Acceso al sistema usuario admin
677	2020-04-12	17:38:17.151713	Acceso al sistema usuario admin
678	2020-04-12	17:42:40.052867	Acceso al sistema usuario admin
679	2020-04-12	17:44:04.807982	Acceso al sistema usuario admin
680	2020-04-12	17:44:23.520458	Acceso al sistema usuario admin
681	2020-04-12	18:25:00.036129	Acceso al sistema usuario admin
682	2020-04-12	18:28:18.094135	Acceso al sistema usuario admin
683	2020-04-12	18:30:14.728735	Acceso al sistema usuario admin
684	2020-04-13	10:39:30.865503	Acceso al sistema usuario admin
685	2020-04-13	10:41:07.140812	Acceso al sistema usuario admin
686	2020-04-13	10:52:18.155693	Acceso al sistema usuario admin
687	2020-04-13	10:55:50.190684	Acceso al sistema usuario admin
688	2020-04-13	10:57:36.417789	Acceso al sistema usuario admin
689	2020-04-13	10:58:51.272715	Acceso al sistema usuario admin
690	2020-04-13	10:59:43.726202	Acceso al sistema usuario admin
691	2020-04-13	11:01:11.721691	Acceso al sistema usuario admin
692	2020-04-13	11:02:08.248972	Acceso al sistema usuario admin
693	2020-04-13	11:15:00.749062	Acceso al sistema usuario admin
694	2020-04-24	14:44:29.109976	Acceso al sistema usuario admin
695	2020-04-24	15:06:28.717885	Acceso al sistema usuario 
696	2020-04-24	15:06:55.197114	Acceso al sistema usuario admin
697	2020-04-24	15:22:59.048544	Acceso al sistema usuario admin
701	2020-04-24	15:27:43.559401	Acceso al sistema usuario admin
702	2020-04-24	21:33:45.282588	Acceso al sistema usuario 
703	2020-06-07	17:48:52.931301	Acceso al sistema usuario admin
704	2020-06-07	17:49:17.686605	Acceso al sistema usuario Admin
705	2020-06-07	17:49:34.502104	Acceso al sistema usuario admin
706	2020-06-07	18:06:02.230608	Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>\n<p>El diseño del sistema, desde su concepción, es descentralizado, conformado por 3 áreas, un Front End, un Back End y la lógica de la base de datos.</p>\n<h5>El FontEnd</h5>\n<p>Esta conformado para ser utilizado en navegadores Web, compatible con Mozilla Firefox y Google Chrome, del mismo se desarrollaron 3 versiones:</p>\n<ul>\n\t<li>HTML5 – JavaScript (Utilizables desde cualquier servidor Web como Apache 2.4, NGINX, etc)</li>\n\t<li>React (Utilizable desde Node.js v-12.X)</li>\n\t<li>Angular (Utilizable desde Node.js v-12.X)</li>\n</ul>\n<p>Consiste en:</p>\n<ul>\n\t<li>Desplegador de paginas de bienvenida, los cuales se pueden editar dentro del mismo sistema, agregar, eliminar y modificar (ABM)</li>\n\t<li>Acceso al sistema, con usuario y clave.</li>\n\t<li>Sistema de menús: está área del sistema, se modifica internamente en la base de datos, y la misma es dependiente de la instalación, esta pensado para el manejo de perfiles, lo que permite que se asignen accesos a cada usuario de forma individual</li>\n\t<li>ABM (Agregar, Borrar, Modificar) de datos: Para las tablas de Parámetros, Vistas y Usuarios, lo que incluye el listado de los mismos.</li>\n</ul>\n<h5>Back End</h5>\n<p>Esta compuesto de un servicio RestFul, que comprende desde el acceso de los usuarios, hasta las actividades de los ABM de las tablas.</p>\n<p>Están desarrollados en 3 idiomas: </p>\n<ul>\n\t<li>PHP 7.2 (Para servidores Apache 2.4 ó NGINX)</li>\n\t<li>Java 8 (Para servidores Tomcat, Wildcat, etc.)</li>\n\t<li>Node.js con Express (Para servidores Node.js)</li>\n</ul>\n<p>El mismo tiene el sistema de verificación JWT (Json Web Token) para cada uno de las solicitudes, respondiendo a través de AJAX.</p>\n<h5>Base de datos</h5>\n<p>Esta desarrollado en PostgreSQL, pero se puede migrar a cualquier base de datos SQL, esta basado en funciones, los cuales son accesibles con un usuario con capacidades limitadas, impidiendo junto con el hecho que no se utilizan comandos SQL directos la inyección de código.</p>\n<p>Para la migración de las bases de datos, se requiere que se nombren con las mismas funciones, recibiendo y enviando los mismos datos.</p> a: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>
707	2020-06-07	18:08:46.387284	Acceso al sistema usuario admin
708	2020-06-07	18:10:29.620078	Inserción de articulo: Codigo, <h1>Código Desarrollado</h1>, <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>
744	2020-11-01	11:38:01.303015	Acceso al sistema usuario admin
745	2020-11-01	11:38:46.139898	Edición de articulo: 1 de: Inicio a: Inicio; <h1>Inicio</h1> a: <h1>Inicio</h1>, de: <h3>Una estructura flexible</h3>\n<p>Estructura de base, que comprende estructura de los usuarios y menús de la aplicación.</p> a: <h3>Una estructura flexible</h3>\n<p>Estructura de base, que comprende estructura de los usuarios y menús de la aplicación.</p>\n<iframe src="rastrahm.com" ></iframe>
709	2020-06-07	18:10:59.107287	Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>
710	2020-06-07	18:13:33.82824	Edición de articulo: 12 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>La inteligencia artificial, es parte de la nueva era</h1>, de: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>
711	2020-06-07	18:14:49.520989	Edición de articulo: 12 de: Codigo a: Codigo; <h1>La inteligencia artificial, es parte de la nueva era</h1> a: <h1>La inteligencia artificial, es parte de la nueva era</h1>, de: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>IA</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>
712	2020-06-07	18:18:55.673983	Edición de articulo: 12 de: Codigo a: Codigo; <h1>La inteligencia artificial, es parte de la nueva era</h1> a: <h1>La inteligencia artificial, es parte de la nueva era</h1>, de: <h3>IA</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>IA</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>
713	2020-06-07	18:19:28.520868	Edición de articulo: 12 de: Codigo a: Codigo1; <h1>La inteligencia artificial, es parte de la nueva era</h1> a: <h1>La inteligencia artificial, es parte de la nueva era</h1>, de: <h3>IA</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>IA</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>
714	2020-06-07	18:23:51.089921	Edición de articulo: 12 de: Codigo1 a: IA; <h1>La inteligencia artificial, es parte de la nueva era</h1> a: <h1>La inteligencia artificial, es parte de la nueva era</h1>, de: <h3>IA</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>IA</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>
715	2020-06-07	18:38:50.813301	Acceso al sistema usuario admin
716	2020-06-07	18:41:46.532063	Acceso al sistema usuario admin
717	2020-06-07	19:20:41.245585	Acceso al sistema usuario admin
718	2020-06-07	19:24:29.812987	Acceso al sistema usuario admin
719	2020-06-07	19:26:00.433602	Acceso al sistema usuario admin
720	2020-06-07	19:35:59.189736	Edición de articulo: 1 de: Inicio a: Codigo; <h1>Desarrollo de Menu Program</h1> a: <h1>Código Desarrollado</h1>, de: <div class="row">\n    <div class="col-3">\n        <div class="card" style="width: 16rem;">\n            <div class="card-body">\n                <h5 class="card-title"> Base de Datos</h5>\n                <p class="card-text">La estructura de la base de datos, esta pensada para contener todo lo referente al sistema (paginas de presentación, gestión de acceso y usuarios, perfiles de usuarios y variables de entorno)  y aparte los datos de la empresa, creando un entorno más seguro para dicha información.</p>\n                <p class="card-text">El contenido de los datos del sistema, consiste en usuarios, claves, perfiles, paginas externas, estructuras de menú, parámetros y los logs de actividad.</p>\n            </div>\n        </div>\n    </div>\n    <div class="col-3">\n        <div class="card" style="width: 16rem;">\n            <div class="card-body">\n                <h5 class="card-title">Back End</h5>\n                <p class="card-text">Creado para ser RestFull, fue desarrollado en varios idiomas para adaptar a diversos entornos: PHP, Node.js, Java (Spark).</p> \n                <p class="card-text">Respondiendo a través de Ajax, utilizando un servidor Apache 2.4, o NGINX configurados como proxys inversos, evitando por este medio el error CORS en los navegadores.</p>\n                <p class="card-text">La verificación se ejecuta a través JWT (Json Web Token) el cual usa los valores del nombre del usuario y el perfil para validar que la conexión sea valida.</p>\n            </div>\n        </div>\n    </div>\n    <div class="col-3">\n        <div class="card" style="width: 16rem;">\n            <div class="card-body">\n                <h5 class="card-title">Front End</h5>\n                <p class="card-text">Las interfaces Web fueron desarrolladas en HTML, React y Angular, a gusto del cliente, verificando la independencia de origen de datos.</p>\n                <p class="card-text">Las ventajas de utilizar interfaces Web representan la independencia del sistema operativo, uso en cualquier plataforma, y la disponibilidad desde cualquier punto que disponga de Internet.</p>\n                <p class="card-text">Además de la disponibilidad y flexibilidad, ofrece la ventaja que al ser separados, podemos desplegar de diversas formas el sistema.</p>\n            </div>\n        </div>\n    </div>\n    <div class="col-3">\n        <div class="card" style="width: 16rem;">\n            <div class="card-body">\n                <h5 class="card-title">Servidores</h5>\n                <p class="card-text">El despliegue de la aplicación, se puede hacer desde cualquier servidor Web (Apache, NGINX) para el Front End HTML-JavaScript, y para el Back End PHP; Node.js con el Front End React y el Back End Express, Tomcat, para el Back End Java.</p>\n                <p class="card-text">Lo que implica la disponibilidad en Windows, Linux y MacOS, tanto para el servidor como para la interfaz frontal de datos.</p>\n                <p class="card-text">Para evitar el CORS, se utiliza como interfaz unica Apache o NGINX configurados como proxys inversos.</p>\n            </div>\n        </div>\n    </div>\n</div> a: <h3>Construcción del Código</h3>\n<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>
721	2020-09-27	16:49:52.143032	Acceso al sistema usuario admin
722	2020-09-27	17:01:48.921583	Acceso al sistema usuario admin
723	2020-09-27	19:59:15.91052	Acceso al sistema usuario admin
724	2020-10-04	09:16:28.785899	Acceso al sistema usuario admin
725	2020-10-04	09:18:15.384868	Acceso al sistema usuario admin
726	2020-10-04	09:31:30.036498	Acceso al sistema usuario admin
727	2020-10-04	09:32:47.209233	Acceso al sistema usuario admin
728	2020-10-04	09:33:17.035009	Acceso al sistema usuario admin
729	2020-10-04	09:35:35.841607	Acceso al sistema usuario admin
730	2020-10-04	18:51:28.889915	Acceso al sistema usuario admin
731	2020-10-04	18:58:12.402503	Acceso al sistema usuario admin
732	2020-10-04	19:00:23.737404	Acceso al sistema usuario admin
733	2020-10-04	19:03:08.242579	Acceso al sistema usuario admin
734	2020-10-04	19:07:21.383089	Acceso al sistema usuario admin
735	2020-10-04	19:16:49.006023	Acceso al sistema usuario admin
736	2020-10-04	19:21:30.812233	Acceso al sistema usuario admin
737	2020-10-04	19:24:27.047617	Acceso al sistema usuario admin
738	2020-10-04	19:25:48.066103	Acceso al sistema usuario admin
739	2020-10-04	19:35:54.571599	Acceso al sistema usuario admin
740	2020-10-11	12:14:25.50841	Acceso al sistema usuario admin
741	2020-10-11	12:19:20.773419	Acceso al sistema usuario admin
742	2020-10-11	12:23:26.292711	Acceso al sistema usuario admin
743	2020-10-11	12:26:19.502756	Acceso al sistema usuario admin
746	2020-11-01	11:39:03.385196	Acceso al sistema usuario admin
747	2020-11-01	11:39:38.132374	Edición de articulo: 1 de: Inicio a: Inicio; <h1>Inicio</h1> a: <h1>Inicio</h1>, de: <h3>Una estructura flexible</h3>\n<p>Estructura de base, que comprende estructura de los usuarios y menús de la aplicación.</p>\n<iframe src="rastrahm.com" ></iframe> a: <h3>Una estructura flexible</h3>\n<p>Estructura de base, que comprende estructura de los usuarios y menús de la aplicación.</p>\n<iframe src="http://rastrahm.com" ></iframe>
748	2020-11-01	11:40:33.22234	Edición de articulo: 1 de: Inicio a: Inicio; <h1>Inicio</h1> a: <h1>Inicio</h1>, de: <h3>Una estructura flexible</h3>\n<p>Estructura de base, que comprende estructura de los usuarios y menús de la aplicación.</p>\n<iframe src="http://rastrahm.com" ></iframe> a: <h3>Una estructura flexible</h3>\n<p>Estructura de base, que comprende estructura de los usuarios y menús de la aplicación.</p>\n<iframe style=""width:100%;"   src="http://rastrahm.com" ></iframe>
749	2020-11-22	14:33:19.607155	Acceso al sistema usuario admin
750	2020-11-22	14:36:39.91402	Acceso al sistema usuario admin
751	2020-11-22	14:36:53.320506	Acceso al sistema usuario admin
752	2020-12-26	12:18:00.577719	Acceso al sistema usuario admin
753	2020-12-26	12:18:15.109328	Acceso al sistema usuario admin
754	2020-12-26	15:26:25.644141	Acceso al sistema usuario admin
755	2020-12-26	15:33:43.050136	Acceso al sistema usuario rastrahm
756	2020-12-26	15:38:03.395031	Acceso al sistema usuario rastrahm
757	2020-12-26	15:38:35.753093	Acceso al sistema usuario 
758	2020-12-26	15:40:30.454652	Acceso al sistema usuario admin
759	2020-12-26	15:54:15.663907	Acceso al sistema usuario admin
760	2020-12-26	15:57:46.103787	Acceso al sistema usuario admin
761	2020-12-26	16:06:13.431788	Acceso al sistema usuario admin
762	2020-12-26	16:15:53.556327	Acceso al sistema usuario admin
763	2020-12-26	16:18:30.861923	Acceso al sistema usuario admin
764	2020-12-26	16:27:04.369153	Acceso al sistema usuario admin
765	2020-12-26	16:27:53.68917	Acceso al sistema usuario admin
766	2020-12-26	19:19:44.122544	Acceso al sistema usuario admin
767	2020-12-26	19:37:11.759815	Acceso al sistema usuario admin
768	2020-12-26	19:42:37.702207	Acceso al sistema usuario admin
769	2020-12-26	19:46:27.019695	Acceso al sistema usuario admin
770	2020-12-26	19:59:50.214025	Acceso al sistema usuario admin
771	2020-12-26	20:09:02.832209	Acceso al sistema usuario admin
772	2020-12-26	20:34:05.557547	Acceso al sistema usuario admin
773	2020-12-26	20:42:39.143194	Acceso al sistema usuario admin
774	2020-12-26	20:52:14.94875	Acceso al sistema usuario admin
775	2020-12-28	16:21:14.740645	Acceso al sistema usuario admin
776	2020-12-28	16:22:13.272257	Acceso al sistema usuario admin
777	2020-12-28	16:26:31.685454	Acceso al sistema usuario admin
778	2020-12-28	16:44:30.820299	Acceso al sistema usuario admin
779	2020-12-28	16:55:23.082959	Acceso al sistema usuario admin
780	2020-12-28	16:58:23.137641	Acceso al sistema usuario admin
781	2020-12-28	16:59:29.746635	Acceso al sistema usuario admin
782	2020-12-28	17:00:51.096163	Acceso al sistema usuario admin
783	2020-12-28	17:03:17.271912	Acceso al sistema usuario admin
784	2020-12-28	17:04:20.113733	Acceso al sistema usuario admin
785	2020-12-28	17:04:44.389361	Acceso al sistema usuario admin
786	2020-12-28	17:09:26.694206	Acceso al sistema usuario admin
787	2020-12-28	17:18:40.977567	Acceso al sistema usuario admin
788	2020-12-28	17:20:32.237233	Acceso al sistema usuario admin
789	2020-12-28	17:26:57.458876	Acceso al sistema usuario admin
790	2020-12-28	17:43:48.894796	Acceso al sistema usuario admin
791	2020-12-28	17:48:56.986247	Acceso al sistema usuario admin
792	2020-12-28	18:25:26.775408	Acceso al sistema usuario admin
793	2020-12-28	18:26:24.918656	Acceso al sistema usuario admin
794	2020-12-28	18:30:49.444196	Acceso al sistema usuario admin
795	2020-12-28	18:31:38.911473	Acceso al sistema usuario admin
796	2020-12-28	18:32:23.546968	Acceso al sistema usuario admin
797	2020-12-28	18:32:46.181377	Acceso al sistema usuario admin
798	2020-12-28	19:01:31.008018	Acceso al sistema usuario admin
799	2020-12-28	19:11:22.171294	Acceso al sistema usuario admin
800	2020-12-28	19:12:13.30659	Acceso al sistema usuario admin
801	2020-12-28	19:12:35.923948	Acceso al sistema usuario admin
802	2020-12-28	19:34:57.561535	Acceso al sistema usuario admin
803	2020-12-28	19:35:15.893366	Acceso al sistema usuario admin
804	2020-12-28	19:35:49.430752	Acceso al sistema usuario admin
805	2020-12-28	19:36:14.422579	Acceso al sistema usuario admin
806	2020-12-28	19:42:42.343993	Acceso al sistema usuario admin
807	2020-12-28	19:43:20.837933	Acceso al sistema usuario admin
808	2020-12-28	19:43:30.659733	Acceso al sistema usuario admin
809	2020-12-28	19:51:49.509325	Acceso al sistema usuario admin
810	2020-12-28	19:53:02.906249	Acceso al sistema usuario admin
811	2020-12-28	19:54:03.901298	Acceso al sistema usuario admin
812	2020-12-28	19:54:25.307358	Acceso al sistema usuario admin
813	2020-12-28	19:55:38.596838	Acceso al sistema usuario admin
814	2020-12-28	19:55:56.536629	Acceso al sistema usuario admin
815	2020-12-28	19:58:54.901241	Acceso al sistema usuario admin
816	2020-12-28	19:59:12.97372	Acceso al sistema usuario admin
817	2020-12-29	10:08:42.862086	Acceso al sistema usuario admin
818	2020-12-29	10:17:40.114822	Acceso al sistema usuario admin
819	2020-12-29	10:20:06.577065	Acceso al sistema usuario admin
820	2020-12-29	10:22:38.66471	Acceso al sistema usuario admin
821	2020-12-29	10:34:05.621871	Acceso al sistema usuario admin
822	2020-12-29	10:35:05.152882	Acceso al sistema usuario admin
823	2020-12-29	10:35:20.779723	Acceso al sistema usuario admin
824	2020-12-29	10:36:03.427886	Acceso al sistema usuario admin
825	2020-12-29	11:00:22.921013	Acceso al sistema usuario admin
826	2020-12-29	11:02:50.951423	Acceso al sistema usuario admin
827	2020-12-29	11:05:18.54628	Acceso al sistema usuario admin
828	2020-12-29	11:08:59.856182	Acceso al sistema usuario admin
829	2020-12-29	11:09:16.558376	Acceso al sistema usuario admin
830	2020-12-29	11:09:25.837356	Acceso al sistema usuario admin
831	2020-12-29	11:09:41.310418	Acceso al sistema usuario admin
832	2020-12-29	11:09:57.686794	Acceso al sistema usuario admin
833	2020-12-29	11:10:00.696216	Acceso al sistema usuario admin
834	2020-12-29	11:12:17.236305	Acceso al sistema usuario admin
835	2020-12-29	11:13:39.574524	Acceso al sistema usuario admin
836	2020-12-29	11:13:49.992831	Acceso al sistema usuario admin
837	2020-12-29	11:14:16.756776	Acceso al sistema usuario admin
838	2020-12-29	11:24:37.600573	Acceso al sistema usuario admin
839	2020-12-29	11:25:00.891028	Acceso al sistema usuario admin
840	2020-12-29	11:33:28.091366	Acceso al sistema usuario admin
841	2020-12-29	11:34:05.591488	Acceso al sistema usuario admin
842	2020-12-29	11:34:51.766999	Acceso al sistema usuario admin
843	2020-12-29	11:35:02.752335	Acceso al sistema usuario admin
844	2020-12-29	11:35:20.434123	Acceso al sistema usuario admin
845	2020-12-29	11:35:34.460389	Acceso al sistema usuario admin
846	2020-12-29	11:35:49.872158	Acceso al sistema usuario admin
847	2020-12-29	11:37:17.454549	Acceso al sistema usuario admin
848	2020-12-29	11:37:41.678389	Acceso al sistema usuario admin
849	2021-01-17	09:49:11.455902	Acceso al sistema usuario admin
850	2021-01-17	10:03:47.762662	Acceso al sistema usuario admin
851	2021-01-17	10:10:06.760706	Acceso al sistema usuario admin
852	2021-01-17	10:31:58.498555	Acceso al sistema usuario admin
853	2021-01-17	11:03:29.318357	Acceso al sistema usuario admin
854	2021-01-17	11:07:28.249781	Acceso al sistema usuario admin
855	2021-01-17	11:09:10.112932	Acceso al sistema usuario admin
856	2021-01-18	17:53:23.614718	Acceso al sistema usuario admin
857	2021-01-18	17:57:50.577352	Acceso al sistema usuario admin
858	2021-01-18	18:03:46.165254	Acceso al sistema usuario admin
859	2021-01-18	18:08:50.610382	Acceso al sistema usuario admin
860	2021-01-18	18:14:18.953619	Acceso al sistema usuario admin
861	2021-01-18	18:17:41.939243	Acceso al sistema usuario admin
862	2021-01-18	18:22:35.550735	Acceso al sistema usuario admin
863	2021-01-18	18:43:22.155514	Acceso al sistema usuario admin
864	2021-01-18	18:43:36.917016	Acceso al sistema usuario admin
865	2021-01-18	18:45:08.052578	Acceso al sistema usuario admin
866	2021-01-18	18:45:25.964538	Acceso al sistema usuario admin
867	2021-01-18	18:45:44.984819	Acceso al sistema usuario admin
868	2021-01-18	18:46:59.813316	Acceso al sistema usuario admin
869	2021-01-19	17:38:25.132458	Acceso al sistema usuario admin
870	2021-01-19	17:46:27.258389	Acceso al sistema usuario admin
871	2021-01-19	17:46:51.161539	Acceso al sistema usuario admin
872	2021-01-19	17:50:30.049467	Acceso al sistema usuario admin
873	2021-01-19	18:04:36.029526	Acceso al sistema usuario admin
874	2021-01-19	18:14:04.926744	Acceso al sistema usuario admin
875	2021-01-19	18:16:05.57084	Acceso al sistema usuario admin
876	2021-01-19	18:27:49.004614	Acceso al sistema usuario admin
877	2021-01-19	18:44:48.443203	Acceso al sistema usuario admin
878	2021-01-19	18:49:21.00447	Acceso al sistema usuario admin
879	2021-01-19	18:53:34.125897	Acceso al sistema usuario admin
880	2021-01-19	19:05:26.65346	Acceso al sistema usuario admin
881	2021-01-19	19:08:18.487022	Acceso al sistema usuario admin
882	2021-01-26	16:36:18.488582	Acceso al sistema usuario admin
883	2021-01-26	16:45:17.573156	Acceso al sistema usuario admin
884	2021-01-26	16:47:03.755923	Acceso al sistema usuario admin
885	2021-01-26	16:57:12.047678	Acceso al sistema usuario admin
886	2021-01-27	17:00:07.909532	Acceso al sistema usuario admin
887	2021-01-27	17:02:48.336074	Acceso al sistema usuario admin
888	2021-01-27	17:04:39.764806	Acceso al sistema usuario admin
889	2021-01-27	17:07:50.994016	Acceso al sistema usuario admin
890	2021-02-02	18:34:17.749523	Acceso al sistema usuario admin
891	2021-02-03	08:52:39.971405	Acceso al sistema usuario 
892	2021-02-05	15:43:16.273741	Acceso al sistema usuario 
893	2021-02-05	16:09:56.34599	Acceso al sistema usuario admin
894	2021-02-05	16:26:21.4443	Acceso al sistema usuario rastrahm
895	2021-02-05	16:55:37.278047	Acceso al sistema usuario admin
898	2021-02-06	09:43:55.891157	Inserción de Usuario: rastrahm, 1
899	2021-02-06	09:43:55.891157	User, insertar Usuario rastrahm, perfil: 1
900	2021-02-06	09:47:43.291452	Inserción de Usuario: admin, 0
901	2021-02-06	09:47:43.291452	User, insertar Usuario admin, perfil: 0
904	2021-02-06	09:48:22.75195	Inserción de Usuario: rastrahm, 1
905	2021-02-06	09:48:22.75195	User, insertar Usuario rastrahm, perfil: 1
910	2021-02-06	10:16:16.697624	Inserción de Usuario: rastrahm, 1
911	2021-02-06	10:16:16.697624	User, insertar Usuario rastrahm, perfil: 1
922	2021-02-06	10:49:19.953071	Inserción de Usuario: rastrahm, 1
923	2021-02-06	10:49:19.953071	User, insertar Usuario rastrahm, perfil: 1
924	2021-02-06	10:58:31.580611	Inserción de Usuario: rastrahm, 1
925	2021-02-06	10:58:31.580611	User, insertar Usuario rastrahm, perfil: 1
926	2021-02-06	11:03:40.133609	Inserción de Usuario: rastrahm, 1
927	2021-02-06	11:03:40.133609	User, insertar Usuario rastrahm, perfil: 1
930	2021-02-06	11:42:56.811704	Inserción de Usuario: rastrahm, 1
931	2021-02-06	11:42:56.811704	User, insertar Usuario rastrahm, perfil: 1
936	2021-02-06	17:23:30.086583	Inserción de Usuario: rastrahm, 1
937	2021-02-06	17:23:30.086583	User, insertar Usuario rastrahm, perfil: 1
938	2021-02-06	17:39:28.507095	Inserción de Usuario: rastrahm, 1
939	2021-02-06	17:39:28.507095	User, insertar Usuario rastrahm, perfil: 1
942	2021-02-06	17:43:00.462138	Inserción de Usuario: rastrahm, 1
943	2021-02-06	17:43:00.462138	User, insertar Usuario rastrahm, perfil: 1
944	2021-02-06	17:46:49.503	Inserción de Usuario: rastrahm, 1
945	2021-02-06	17:46:49.503	User, insertar Usuario rastrahm, perfil: 1
946	2021-02-06	17:52:08.077291	Inserción de Usuario: rastrahm, 1
947	2021-02-06	17:52:08.077291	User, insertar Usuario rastrahm, perfil: 1
948	2021-02-08	14:36:27.785421	Inserción de Usuario: rastrahm, 1
949	2021-02-08	14:36:27.785421	User, insertar Usuario rastrahm, perfil: 1
950	2021-02-08	14:45:21.581629	Inserción de Usuario: rastrahm, 1
951	2021-02-08	14:45:21.581629	User, insertar Usuario rastrahm, perfil: 1
952	2021-02-08	15:03:55.499742	Inserción de Usuario: rastrahm, 1
953	2021-02-08	15:03:55.499742	User, insertar Usuario rastrahm, perfil: 1
954	2021-02-08	16:47:58.848239	Inserción de Usuario: rastrahm, 1
955	2021-02-08	16:47:58.848239	User, insertar Usuario rastrahm, perfil: 1
956	2021-02-09	17:17:08.324375	Inserción de Usuario: rastrahm, 1
957	2021-02-09	17:17:08.324375	User, insertar Usuario rastrahm, perfil: 1
961	2021-02-09	17:38:41.277912	Activación de usuario rastrahm
962	2021-02-09	17:38:41.277912	User, actualización Usuario: rastrahm, Activo: 1, Perfil: 1
963	2021-02-10	18:37:08.000937	Acceso al sistema usuario admin
964	2021-02-10	18:38:08.613219	Acceso al sistema usuario admin
965	2021-02-10	18:39:19.648081	Acceso al sistema usuario admin
966	2021-02-10	18:49:52.931921	Acceso al sistema usuario admin
967	2021-02-10	18:52:08.554464	Acceso al sistema usuario admin
968	2021-02-11	08:18:31.314162	Acceso al sistema usuario admin
969	2021-02-11	09:55:58.691831	Acceso al sistema usuario admin
970	2021-02-11	09:59:24.860707	Acceso al sistema usuario admin
971	2021-02-11	10:02:24.999201	Acceso al sistema usuario admin
972	2021-02-11	10:07:54.630001	Acceso al sistema usuario admin
973	2021-02-11	10:11:58.699591	Acceso al sistema usuario admin
974	2021-02-11	10:14:51.998861	Acceso al sistema usuario admin
975	2021-02-11	10:19:12.927651	Acceso al sistema usuario admin
976	2021-02-11	10:20:50.138842	Acceso al sistema usuario admin
977	2021-02-11	10:21:38.097277	Acceso al sistema usuario admin
978	2021-02-11	10:25:48.183527	Acceso al sistema usuario admin
979	2021-02-11	10:31:18.071809	Acceso al sistema usuario admin
980	2021-02-11	10:34:24.748502	Acceso al sistema usuario admin
981	2021-02-11	10:38:04.380469	Acceso al sistema usuario admin
982	2021-02-11	10:39:54.748184	Acceso al sistema usuario admin
983	2021-02-11	10:50:37.348819	Acceso al sistema usuario admin
984	2021-02-11	11:24:51.748183	Acceso al sistema usuario admin
985	2021-02-11	14:12:47.135513	Acceso al sistema usuario admin
986	2021-02-11	14:33:45.01976	Acceso al sistema usuario admin
987	2021-02-11	14:34:06.755293	Acceso al sistema usuario admin
988	2021-02-11	14:34:39.417687	Acceso al sistema usuario admin
989	2021-02-11	14:37:03.307122	Acceso al sistema usuario admin
990	2021-02-11	15:47:53.385049	Acceso al sistema usuario admin
991	2021-02-12	08:07:39.15764	Acceso al sistema usuario admin
992	2021-02-12	08:32:05.391167	Acceso al sistema usuario admin
993	2021-02-12	08:33:32.463564	Acceso al sistema usuario admin
994	2021-02-12	09:03:52.575818	Acceso al sistema usuario admin
995	2021-02-12	09:18:52.908315	Acceso al sistema usuario admin
996	2021-02-12	09:29:54.767863	Acceso al sistema usuario admin
997	2021-02-12	09:38:54.361336	Acceso al sistema usuario admin
998	2021-02-12	09:45:52.811044	Acceso al sistema usuario admin
999	2021-02-12	09:54:34.043662	Acceso al sistema usuario admin
1000	2021-02-12	10:12:23.205967	Acceso al sistema usuario admin
1001	2021-02-12	10:23:33.719866	Acceso al sistema usuario admin
1002	2021-02-12	10:34:05.68718	Acceso al sistema usuario admin
1003	2021-02-12	10:40:31.518208	Acceso al sistema usuario admin
1004	2021-02-12	10:49:13.153134	Acceso al sistema usuario admin
1005	2021-02-12	10:53:31.345165	Acceso al sistema usuario admin
1006	2021-02-12	11:02:15.099065	Acceso al sistema usuario admin
1007	2021-02-12	11:17:59.26253	Acceso al sistema usuario admin
1008	2021-02-12	11:31:36.67545	Acceso al sistema usuario admin
1009	2021-02-12	11:42:10.378355	Acceso al sistema usuario admin
1010	2021-02-12	11:43:52.11639	Acceso al sistema usuario admin
1011	2021-02-12	11:44:09.525637	Acceso al sistema usuario admin
1012	2021-02-12	11:45:10.293906	Acceso al sistema usuario admin
1013	2021-02-12	11:45:57.603931	Acceso al sistema usuario admin
1014	2021-02-12	11:52:42.604749	Acceso al sistema usuario admin
1015	2021-02-12	11:56:35.835	Acceso al sistema usuario admin
1016	2021-02-12	12:03:25.422837	Acceso al sistema usuario admin
1017	2021-02-12	12:15:54.170813	Acceso al sistema usuario admin
1018	2021-02-12	12:22:54.32527	Acceso al sistema usuario admin
1019	2021-02-12	12:24:16.566955	Acceso al sistema usuario admin
1020	2021-02-12	12:28:00.033564	Acceso al sistema usuario admin
1021	2021-02-12	12:32:48.807264	Acceso al sistema usuario admin
1022	2021-02-12	12:34:07.346504	Acceso al sistema usuario admin
1023	2021-02-12	12:38:35.42536	Acceso al sistema usuario admin
1024	2021-02-12	12:40:44.879922	Acceso al sistema usuario admin
1025	2021-02-12	12:42:24.679944	Acceso al sistema usuario admin
1026	2021-02-12	13:12:36.637793	Acceso al sistema usuario admin
1027	2021-02-12	13:16:55.472569	Acceso al sistema usuario admin
1028	2021-02-12	13:18:21.793037	Acceso al sistema usuario admin
1029	2021-02-12	14:03:23.197257	Acceso al sistema usuario admin
1030	2021-02-12	14:05:14.045716	Acceso al sistema usuario admin
1031	2021-02-12	14:10:44.9706	Acceso al sistema usuario admin
1032	2021-02-12	14:14:50.770063	Acceso al sistema usuario admin
1033	2021-02-12	14:31:20.704186	Acceso al sistema usuario admin
1034	2021-02-12	14:33:05.776484	Acceso al sistema usuario admin
1035	2021-02-12	14:37:44.52856	Acceso al sistema usuario admin
1036	2021-02-12	14:44:10.364233	Acceso al sistema usuario admin
1037	2021-02-12	14:44:59.70645	Acceso al sistema usuario admin
1038	2021-02-12	14:47:11.668224	Acceso al sistema usuario admin
1039	2021-02-12	14:51:03.285104	Acceso al sistema usuario admin
1040	2021-02-12	15:08:04.654339	Acceso al sistema usuario admin
1041	2021-02-12	15:17:55.599187	Acceso al sistema usuario admin
1042	2021-02-12	15:21:50.248796	Acceso al sistema usuario admin
1043	2021-02-12	15:24:25.988466	Acceso al sistema usuario admin
1044	2021-02-12	15:26:23.893047	Acceso al sistema usuario admin
1045	2021-02-12	15:26:59.70752	Acceso al sistema usuario admin
1046	2021-02-12	15:27:49.989112	Acceso al sistema usuario admin
1047	2021-02-12	15:28:11.753001	Acceso al sistema usuario admin
1048	2021-02-12	15:35:21.957584	Acceso al sistema usuario admin
1049	2021-02-12	15:38:22.045052	Acceso al sistema usuario admin
1050	2021-02-12	15:41:59.987719	Acceso al sistema usuario admin
1051	2021-02-12	15:47:26.686689	Acceso al sistema usuario admin
1052	2021-02-12	15:55:53.091993	Acceso al sistema usuario admin
1053	2021-02-13	08:15:54.323758	Acceso al sistema usuario admin
1054	2021-02-13	08:23:05.872997	Acceso al sistema usuario admin
1055	2021-02-13	08:27:00.703104	Acceso al sistema usuario admin
1056	2021-02-13	08:28:49.373462	Acceso al sistema usuario admin
1057	2021-02-13	08:47:56.618902	Acceso al sistema usuario admin
1058	2021-02-13	08:55:39.87745	Acceso al sistema usuario admin
1059	2021-02-13	09:05:09.679905	Acceso al sistema usuario admin
1060	2021-02-13	09:17:42.073092	Acceso al sistema usuario admin
1061	2021-02-13	09:22:12.163755	Acceso al sistema usuario admin
1062	2021-02-13	09:33:20.3309	Acceso al sistema usuario admin
1063	2021-02-13	09:43:15.512829	Acceso al sistema usuario admin
1064	2021-02-13	09:50:56.759415	Acceso al sistema usuario admin
1065	2021-02-13	09:54:59.21374	Acceso al sistema usuario admin
1066	2021-02-13	10:20:08.18948	Acceso al sistema usuario admin
1067	2021-02-13	11:37:52.059358	Acceso al sistema usuario admin
1068	2021-02-13	11:39:36.62631	Acceso al sistema usuario admin
1069	2021-02-13	11:40:30.837685	Acceso al sistema usuario admin
1070	2021-02-13	11:42:00.495807	Acceso al sistema usuario admin
1071	2021-02-13	11:43:50.390665	Acceso al sistema usuario admin
1072	2021-02-13	11:45:24.460738	Acceso al sistema usuario admin
1073	2021-02-13	11:49:16.19965	Acceso al sistema usuario admin
1074	2021-02-13	13:42:15.739233	Acceso al sistema usuario admin
1075	2021-02-13	13:43:25.55863	Acceso al sistema usuario admin
1076	2021-02-13	13:45:08.623497	Acceso al sistema usuario admin
1077	2021-02-13	13:59:11.784435	Acceso al sistema usuario admin
1078	2021-02-13	13:59:58.387149	Acceso al sistema usuario admin
1079	2021-02-13	14:00:39.251771	Acceso al sistema usuario admin
1080	2021-02-13	14:02:29.000098	Acceso al sistema usuario admin
1081	2021-02-13	14:06:21.947624	Acceso al sistema usuario admin
1082	2021-02-13	14:09:30.373459	Acceso al sistema usuario admin
1083	2021-02-13	14:11:06.335311	Acceso al sistema usuario admin
1084	2021-02-13	14:21:39.17848	Acceso al sistema usuario admin
1085	2021-02-13	14:24:42.744176	Acceso al sistema usuario admin
1086	2021-02-13	14:28:21.571044	Acceso al sistema usuario admin
1087	2021-02-13	14:30:38.049956	Acceso al sistema usuario admin
1088	2021-02-13	14:39:07.753801	Acceso al sistema usuario admin
1089	2021-02-13	14:40:40.499665	Acceso al sistema usuario admin
1090	2021-02-13	14:42:46.929141	Acceso al sistema usuario admin
1091	2021-02-13	14:44:20.151452	Acceso al sistema usuario admin
1092	2021-02-13	14:53:02.394855	Acceso al sistema usuario admin
1093	2021-02-13	14:56:43.394877	Acceso al sistema usuario admin
1094	2021-02-13	15:01:32.719256	Acceso al sistema usuario admin
1095	2021-02-13	15:05:32.784871	Acceso al sistema usuario admin
1096	2021-02-13	15:06:57.151662	Acceso al sistema usuario admin
1097	2021-02-13	15:10:12.13094	Acceso al sistema usuario admin
1098	2021-02-13	15:13:42.95138	Acceso al sistema usuario admin
1099	2021-02-13	15:16:46.139298	Acceso al sistema usuario admin
1100	2021-02-13	15:21:51.916471	Acceso al sistema usuario admin
1101	2021-02-13	15:23:08.614761	Acceso al sistema usuario admin
1102	2021-02-13	15:24:29.238889	Acceso al sistema usuario admin
1103	2021-02-13	16:22:37.877798	Acceso al sistema usuario admin
1104	2021-02-13	16:25:10.990743	Acceso al sistema usuario admin
1105	2021-02-13	16:27:02.683129	Acceso al sistema usuario admin
1106	2021-02-13	16:29:05.227742	Acceso al sistema usuario admin
1107	2021-02-13	16:30:01.283183	Acceso al sistema usuario admin
1108	2021-02-13	16:32:32.830606	Acceso al sistema usuario admin
1109	2021-02-13	16:37:20.987295	Acceso al sistema usuario admin
1110	2021-02-13	16:39:41.095111	Acceso al sistema usuario admin
1111	2021-02-13	16:40:50.952656	Acceso al sistema usuario admin
1112	2021-02-13	17:02:57.938735	Acceso al sistema usuario admin
1113	2021-02-14	16:40:44.879501	Acceso al sistema usuario admin
1114	2021-02-14	16:41:34.471947	Acceso al sistema usuario admin
1115	2021-02-14	16:47:07.276162	Acceso al sistema usuario admin
1116	2021-02-14	16:55:13.048638	Acceso al sistema usuario admin
1117	2021-02-14	17:06:21.556485	Acceso al sistema usuario admin
1118	2021-02-14	17:18:23.93794	Acceso al sistema usuario admin
1119	2021-02-14	17:20:22.177561	Acceso al sistema usuario admin
1120	2021-02-14	17:33:54.401176	Acceso al sistema usuario admin
1121	2021-02-14	17:35:35.358728	Acceso al sistema usuario admin
1122	2021-02-14	17:51:22.530736	Acceso al sistema usuario admin
1123	2021-02-14	17:53:10.010483	Acceso al sistema usuario admin
1124	2021-02-14	17:56:33.483355	Acceso al sistema usuario admin
1125	2021-02-15	10:39:59.181472	Acceso al sistema usuario admin
1126	2021-02-15	10:49:15.105207	Acceso al sistema usuario admin
1127	2021-02-15	10:52:45.868631	Acceso al sistema usuario admin
1128	2021-02-15	11:07:34.492074	Acceso al sistema usuario admin
1129	2021-02-15	11:14:41.033471	Acceso al sistema usuario admin
1130	2021-02-15	11:56:24.222152	Acceso al sistema usuario admin
1131	2021-02-15	12:02:09.902722	Acceso al sistema usuario admin
1132	2021-02-15	12:08:10.097852	Acceso al sistema usuario admin
1133	2021-02-15	12:10:55.761691	Acceso al sistema usuario admin
1134	2021-02-15	16:09:51.958887	Acceso al sistema usuario admin
1135	2021-02-15	16:13:39.287626	Acceso al sistema usuario admin
1136	2021-02-15	16:16:01.667922	Acceso al sistema usuario admin
1137	2021-02-15	16:18:22.414567	Acceso al sistema usuario admin
1138	2021-02-15	16:19:38.020646	Acceso al sistema usuario admin
1139	2021-02-15	16:32:02.756386	Acceso al sistema usuario admin
1140	2021-02-16	08:54:11.411369	Acceso al sistema usuario admin
1141	2021-02-16	08:55:49.140171	Acceso al sistema usuario admin
1142	2021-02-16	09:01:01.846317	Acceso al sistema usuario admin
1143	2021-02-16	09:09:52.674725	Acceso al sistema usuario admin
1144	2021-02-16	09:15:35.107858	Acceso al sistema usuario admin
1145	2021-02-16	09:34:19.052208	Acceso al sistema usuario admin
1146	2021-02-16	10:16:01.096345	Acceso al sistema usuario admin
1147	2021-02-16	10:18:37.382977	Acceso al sistema usuario admin
1148	2021-02-16	10:28:23.940115	Acceso al sistema usuario admin
1149	2021-02-16	10:43:18.850206	Acceso al sistema usuario admin
1150	2021-02-16	10:50:28.882058	Acceso al sistema usuario admin
1151	2021-02-16	10:52:36.706402	Acceso al sistema usuario admin
1152	2021-02-16	10:54:43.118819	Acceso al sistema usuario admin
1153	2021-02-16	11:49:49.62091	Acceso al sistema usuario admin
1154	2021-02-16	13:35:01.645236	Acceso al sistema usuario admin
1155	2021-02-16	13:37:00.020133	Acceso al sistema usuario admin
1156	2021-02-16	13:43:18.68694	Acceso al sistema usuario admin
1157	2021-02-16	13:45:20.650694	Acceso al sistema usuario admin
1158	2021-02-16	13:58:20.065956	Acceso al sistema usuario admin
1159	2021-02-16	14:13:38.646722	Acceso al sistema usuario admin
1160	2021-02-16	14:39:54.396527	Acceso al sistema usuario admin
1161	2021-02-16	14:43:28.788508	Acceso al sistema usuario admin
1162	2021-02-16	14:48:06.314	Acceso al sistema usuario admin
1163	2021-02-16	15:01:50.403261	Acceso al sistema usuario admin
1164	2021-02-16	15:02:33.283762	Acceso al sistema usuario admin
1165	2021-02-16	15:17:33.340199	Acceso al sistema usuario admin
1166	2021-02-16	15:30:11.766975	Acceso al sistema usuario admin
1167	2021-02-16	15:32:37.96234	Acceso al sistema usuario admin
1168	2021-02-16	16:03:01.951402	Acceso al sistema usuario admin
1169	2021-02-16	16:04:26.086743	Acceso al sistema usuario admin
1170	2021-02-16	16:08:10.160731	Acceso al sistema usuario admin
1171	2021-02-16	16:12:35.911749	Acceso al sistema usuario admin
1172	2021-02-17	10:10:56.606183	Acceso al sistema usuario admin
1173	2021-02-17	17:48:18.931633	Acceso al sistema usuario admin
1174	2021-02-17	18:06:36.817218	Acceso al sistema usuario admin
1175	2021-02-17	18:09:28.479808	Acceso al sistema usuario admin
1176	2021-02-17	18:18:47.436505	Acceso al sistema usuario admin
1177	2021-02-17	18:27:17.643883	Acceso al sistema usuario admin
1178	2021-02-17	18:36:17.985102	Acceso al sistema usuario admin
1179	2021-02-17	18:37:12.471788	Acceso al sistema usuario admin
1180	2021-02-17	18:40:13.476698	Acceso al sistema usuario admin
1181	2021-02-20	09:22:03.241534	Acceso al sistema usuario admin
1182	2021-02-20	09:36:49.205904	Acceso al sistema usuario admin
1183	2021-02-20	09:37:41.074471	Acceso al sistema usuario admin
1184	2021-02-20	09:53:56.505668	Acceso al sistema usuario admin
1185	2021-02-20	10:04:30.986897	Acceso al sistema usuario admin
1186	2021-02-21	14:24:50.840452	Acceso al sistema usuario 
1187	2021-02-21	14:29:18.897302	Acceso al sistema usuario 
1188	2021-02-21	14:48:49.574325	Acceso al sistema usuario admin
1189	2021-02-21	14:59:29.790563	Acceso al sistema usuario admin
1190	2021-02-21	15:04:37.302799	Acceso al sistema usuario admin
1191	2021-02-21	15:05:56.749865	Acceso al sistema usuario admin
1192	2021-02-21	15:06:29.09428	Acceso al sistema usuario admin
1193	2021-02-21	15:56:19.880819	Acceso al sistema usuario admin
1194	2021-02-21	16:06:14.775035	Acceso al sistema usuario admin
1195	2021-02-21	16:10:02.526186	Cambio de parametro: 1, LOGO de valor /var/pruebaJava a /var/listtask
1196	2021-02-21	16:43:31.435952	Acceso al sistema usuario admin
1210	2021-02-21	17:19:38.754799	Inserción de parametro: val, val
1211	2021-03-01	10:47:46.769605	Acceso al sistema usuario admin
1212	2021-03-01	10:58:28.005638	Acceso al sistema usuario admin
1213	2021-03-01	10:59:45.803079	Acceso al sistema usuario admin
1214	2021-03-01	11:00:43.266542	Acceso al sistema usuario admin
1215	2021-03-01	11:26:03.217754	Acceso al sistema usuario admin
1216	2021-03-01	11:27:53.335898	Acceso al sistema usuario admin
1217	2021-03-01	11:30:40.449606	Acceso al sistema usuario admin
1218	2021-03-01	11:30:54.784667	Acceso al sistema usuario admin
1219	2021-03-01	11:32:24.408913	Acceso al sistema usuario admin
1220	2021-03-01	11:32:34.679639	Acceso al sistema usuario admin
1221	2021-03-01	11:32:36.064106	Acceso al sistema usuario admin
1222	2021-03-01	11:32:36.955566	Acceso al sistema usuario admin
1223	2021-03-01	11:32:42.799013	Acceso al sistema usuario admin
1224	2021-03-01	11:32:43.868189	Acceso al sistema usuario admin
1225	2021-03-01	11:36:08.0312	Acceso al sistema usuario admin
1226	2021-03-01	11:36:21.008226	Acceso al sistema usuario admin
1227	2021-03-01	11:36:44.198094	Acceso al sistema usuario admin
1228	2021-03-01	11:38:13.747741	Acceso al sistema usuario admin
1229	2021-03-01	11:42:01.396898	Acceso al sistema usuario addmin
1230	2021-03-01	11:43:31.020538	Acceso al sistema usuario admin
1231	2021-03-01	11:48:08.596574	Acceso al sistema usuario admin
1232	2021-03-01	11:48:57.875994	Acceso al sistema usuario admin
1233	2021-03-01	11:49:34.505114	Acceso al sistema usuario admin
1234	2021-03-01	11:57:57.706577	Acceso al sistema usuario admin
1235	2021-03-01	12:01:46.469155	Acceso al sistema usuario admin
1236	2021-03-01	13:23:40.825219	Acceso al sistema usuario admin
1237	2021-03-01	13:28:00.692685	Acceso al sistema usuario admin
1238	2021-03-01	13:36:39.458282	Acceso al sistema usuario admin
1239	2021-03-01	13:40:17.907105	Acceso al sistema usuario admin
1240	2021-03-01	13:41:18.87071	Acceso al sistema usuario admin
1241	2021-03-01	13:48:57.333712	Acceso al sistema usuario admin
1242	2021-03-01	13:53:10.133906	Acceso al sistema usuario admin
1243	2021-03-01	13:55:30.949572	Acceso al sistema usuario admin
1244	2021-03-01	13:58:02.97385	Acceso al sistema usuario admin
1245	2021-03-01	13:59:49.945745	Acceso al sistema usuario admin
1246	2021-03-01	14:03:58.044155	Acceso al sistema usuario admin
1247	2021-03-01	14:05:07.814466	Acceso al sistema usuario admin
1248	2021-03-01	14:16:06.059408	Acceso al sistema usuario admin
1249	2021-03-01	14:17:21.970663	Acceso al sistema usuario admin
1250	2021-03-01	14:23:00.222266	Acceso al sistema usuario admin
1251	2021-03-01	14:26:15.318155	Acceso al sistema usuario admin
1252	2021-03-01	14:31:34.266078	Acceso al sistema usuario admin
1253	2021-03-01	14:32:51.785632	Acceso al sistema usuario admin
1254	2021-03-01	14:54:08.008166	Acceso al sistema usuario admin
1255	2021-03-01	14:57:36.148605	Acceso al sistema usuario admin
1256	2021-03-01	14:58:43.480829	Acceso al sistema usuario admin
1257	2021-03-01	15:01:29.084969	Acceso al sistema usuario admin
1258	2021-03-01	15:10:18.100835	Acceso al sistema usuario admin
1259	2021-03-01	15:16:45.675185	Acceso al sistema usuario admin
1260	2021-03-01	16:34:40.428274	Acceso al sistema usuario admin
1261	2021-03-01	16:43:22.563752	Acceso al sistema usuario admin
1262	2021-03-01	16:52:23.321548	Acceso al sistema usuario admin
1263	2021-03-01	16:55:06.408318	Acceso al sistema usuario admin
1264	2021-03-01	16:59:10.262771	Acceso al sistema usuario admin
1265	2021-03-01	17:00:56.638097	Acceso al sistema usuario admin
1266	2021-03-01	17:09:29.454831	Acceso al sistema usuario admin
1267	2021-03-01	17:15:05.304088	Acceso al sistema usuario admin
1268	2021-03-01	17:21:55.545182	Acceso al sistema usuario admin
1269	2021-03-01	17:25:26.785234	Acceso al sistema usuario admin
1270	2021-03-01	17:28:42.123745	Acceso al sistema usuario admin
1271	2021-03-01	17:30:30.289979	Acceso al sistema usuario admin
1272	2021-03-01	17:33:37.596796	Acceso al sistema usuario admin
1273	2021-03-01	17:36:58.577298	Acceso al sistema usuario admin
1274	2021-03-01	18:18:35.619111	Acceso al sistema usuario admin
1275	2021-03-01	18:36:36.374633	Acceso al sistema usuario admin
1276	2021-03-01	18:49:15.884702	Acceso al sistema usuario admin
1277	2021-03-01	18:55:36.392126	Acceso al sistema usuario admin
1278	2021-03-01	18:59:32.06619	Acceso al sistema usuario admin
1279	2021-03-07	08:44:50.378139	Acceso al sistema usuario admin
1280	2021-03-07	09:09:29.481229	Acceso al sistema usuario admin
1281	2021-03-07	09:35:59.392102	Acceso al sistema usuario admin
1282	2021-03-07	09:37:10.644255	Acceso al sistema usuario admin
1283	2021-03-07	09:37:51.020956	Acceso al sistema usuario admin
1284	2021-03-07	09:39:40.490995	Acceso al sistema usuario admin
1285	2021-03-07	09:42:46.004396	Acceso al sistema usuario admin
1286	2021-03-07	09:43:02.266534	Acceso al sistema usuario admin
1287	2021-03-07	09:46:22.153471	Acceso al sistema usuario admin
1288	2021-03-07	09:52:20.625093	Acceso al sistema usuario admin
1289	2021-03-07	10:03:10.584772	Acceso al sistema usuario admin
1290	2021-03-07	10:03:53.823481	Acceso al sistema usuario admin
1291	2021-03-07	10:07:54.21823	Acceso al sistema usuario admin
1292	2021-03-07	10:11:04.501045	Acceso al sistema usuario admin
1293	2021-03-07	10:23:28.743778	Acceso al sistema usuario admin
1294	2021-03-07	10:25:13.619023	Acceso al sistema usuario admin
1295	2021-03-07	10:39:43.563413	Acceso al sistema usuario admin
1296	2021-03-07	10:40:41.855597	Acceso al sistema usuario admin
1297	2021-03-07	10:41:50.038278	Acceso al sistema usuario admin
1298	2021-03-07	10:45:41.206398	Acceso al sistema usuario admin
1299	2021-03-07	10:46:25.912098	Acceso al sistema usuario admin
1300	2021-03-07	10:49:39.89481	Acceso al sistema usuario admin
1301	2021-03-07	10:52:15.163275	Acceso al sistema usuario admin
1302	2021-03-07	10:52:50.45117	Acceso al sistema usuario admin
1303	2021-03-07	14:29:54.402692	Acceso al sistema usuario admin
1304	2021-03-07	14:43:39.47201	Acceso al sistema usuario admin
1305	2021-03-07	14:47:13.59735	Acceso al sistema usuario admin
1306	2021-03-07	14:57:22.19926	Acceso al sistema usuario admin
1307	2021-03-07	14:57:53.475999	Acceso al sistema usuario admin
1308	2021-03-07	15:02:09.525597	Acceso al sistema usuario admin
1309	2021-03-07	16:31:45.537315	Acceso al sistema usuario admin
1310	2021-03-07	16:41:46.087123	Acceso al sistema usuario admin
1311	2021-03-07	17:06:54.303226	Acceso al sistema usuario admin
1312	2021-03-07	17:22:41.665583	Acceso al sistema usuario admin
1313	2021-03-07	17:23:28.53313	Acceso al sistema usuario admin
1314	2021-03-07	17:24:58.960271	Acceso al sistema usuario admin
1315	2021-03-07	17:26:25.648242	Acceso al sistema usuario admin
1316	2021-03-07	17:29:15.799958	Acceso al sistema usuario admin
1317	2021-03-07	17:31:18.84218	Acceso al sistema usuario admin
1318	2021-03-07	17:33:33.679324	Cambio de parametro: 4, LOGO de valor /var/pruebaJava a listasta
1319	2021-03-07	17:38:33.815913	Acceso al sistema usuario admin
1320	2021-03-07	17:39:10.980928	Cambio de parametro: 4, LOGO de valor listasta a prueba 2
1321	2021-03-07	17:46:38.353632	Acceso al sistema usuario admin
1322	2021-03-07	17:47:02.413965	Cambio de parametro: 4, LOGO de valor prueba 2 a cambio 3
1323	2021-03-07	17:50:05.788937	Acceso al sistema usuario admin
1324	2021-03-07	17:50:46.793386	Cambio de parametro: 4, LOGO de valor cambio 3 a cambio 4
1325	2021-03-07	17:52:04.193121	Cambio de parametro: 4, LOGO de valor cambio 4 a cambio 5
1326	2021-03-07	17:53:14.11083	Acceso al sistema usuario admin
1327	2021-03-07	17:53:34.914969	Cambio de parametro: 4, LOGO de valor cambio 5 a cambio 6
1328	2021-03-07	17:57:26.077411	Acceso al sistema usuario admin
1329	2021-03-07	17:57:50.245272	Cambio de parametro: 4, LOGO de valor cambio 6 a cambio 7
1330	2021-03-07	17:59:56.249858	Acceso al sistema usuario admin
1331	2021-03-07	18:00:15.301248	Cambio de parametro: 4, LOGO de valor cambio 7 a cambio 8
\.


--
-- TOC entry 3175 (class 0 OID 83666)
-- Dependencies: 207
-- Data for Name: menu; Type: TABLE DATA; Schema: program; Owner: rasg
--

COPY program.menu (men_id, pro_id, men_name, men_description) FROM stdin;
1	0	Archivos	Acceso a las opcines de archivos
4	0	Ayuda	Menú de ayuda
3	0	Preferencias	Opciones del sistema
2	0	ListTask	Trabajos con el Listask
5	1	Archivos	Acceso a las opcines de archivos
6	1	ListTask	Trabajos con el Listask
7	1	Ayuda	Menú de ayuda
\.


--
-- TOC entry 3177 (class 0 OID 83674)
-- Dependencies: 209
-- Data for Name: menu_options; Type: TABLE DATA; Schema: program; Owner: rasg
--

COPY program.menu_options (mop_id, men_id, pro_id, mop_name, mop_description, mop_shortcut, mop_active, mop_object, mop_data_function, mop_data_parameters, mop_action) FROM stdin;
9	1	0	Salir	Termina la aplicación	Alt + F4	t	\N	\N	\N	exit
20	4	0	Ayuda	Indice de ayuda	F1	t	\N	\N	\N	help
21	4	0	Acerca de	Acerca de la aplicacion	\N	t	\N	\N	\N	about
16	3	0	Opciones	Opciones del sistema	\N	t	objects	fnc_parameter	{'str_name','txt_value','bol_visible'}	parameters
17	3	0	Usuarios	Creación y modificación de usuarios	\N	t	objects	fnc_users	{'str_name','bol_active','big_pro','str_email','str_pass'}	users
24	5	1	Salir	Termina la aplicación	Alt+F4	t	\N	\N	\N	exit
26	6	1	Json	Editar valores en formato Json	\N	t	\N	\N	\N	Json
27	7	1	Ayuda	Indice de ayuda	F1	t	\N	\N	\N	help
28	7	1	Acerca de	Acerca de la aplicacion	\N	t	\N	\N	\N	about
1	3	0	Articulos 	Edición de los articulos externos del sistema	\N	t	objects	fnc_previews	{'str_link','txt_title','txt_article'}	previews
23	2	0	Json	Editar valores en formato Json	\N	t	objects	fnc_grouptask	{'big_usr','txt_json'}	Json
25	6	1	ListTask	Edición de Valores 	\N	t	\N	\N	\N	listtask
22	2	0	ListTask	Edición de Valores 	\N	t	objects	fnc_grouptask	{'big_usr','txt_json'}	listtask
\.


--
-- TOC entry 3182 (class 0 OID 83689)
-- Dependencies: 214
-- Data for Name: parameter; Type: TABLE DATA; Schema: program; Owner: postgres
--

COPY program.parameter (par_id, par_name, par_value, par_visible) FROM stdin;
1	APP_WIDTH	600px	1
2	APP_HEIGHT	400px	1
3	JWT_KEY	Sistema Sistema Sistema Sistema Sistema Sistema Sistema SistemaSistema Sistema Sistema Sistema v Sistema Sistema Sistema Sistema Sistema Sistema Sistema Sistema Sistema v Sistema Sistema v Sistema Sistema Sistema Sistema v Sistema Sistema Sistema Sistemav	0
5	val	val	1
4	LOGO	cambio 8	1
\.


--
-- TOC entry 3184 (class 0 OID 83698)
-- Dependencies: 216
-- Data for Name: preview; Type: TABLE DATA; Schema: program; Owner: rasg
--

COPY program.preview (pre_id, pre_title, pre_article, pre_link) FROM stdin;
1	<h1>Inicio</h1>	<h1>ListTask</h1>\n<h2>Concepto</h2>\n<p>Sistema que permite crear tareas, y agruparlas, diseñado para ejecutarse en un escritorio, en sistemas Android, y en el presente entorno web.</p>\n<h2>Usuario</h2>\n<p>Para utilizar el back-end del sistema se debe registrar el usuario en esta pagina, para posteriormente registrar en el sistema, tanto en Android como en el escritorio, la ruta corresponde [ruta servidor]/listtask, el resto de la rutas internas se encargan del manejo.</p>\n<h3>Fuentes</h3>\n<p>Los códigos fuentes de las aplicaciones se encuentran en bitbucket:</p>\n<h4>Escritorio</h4>\n<pre>git clone https://rastrahm@bitbucket.org/rastrahm/listtaskdesktop.git</pre>\n<h4>Android</h4>\n<pre>git clone https://rastrahm@bitbucket.org/rastrahm/listtaskandroid.git</pre>\n<h4>React</h4>\n<pre>git clone https://rastrahm@bitbucket.org/rastrahm/listtaskreact.git</pre>\n<h4>Server PHP</h4>\n<pre>git clone https://rastrahm@bitbucket.org/rastrahm/listtaskserver.git</pre>\n	Inicio
3	<h1>Descargas</h1>	<h1>ListTask</h1>\n<h2>Preconpilados</h2>\n<h4>Android App</h4>\n\n<h4>Desktop</h4>\n\n<h2>Código fuente</h2>\n<h4>Escritorio</h4>\n<pre>git clone https://rastrahm@bitbucket.org/rastrahm/listtaskdesktop.git</pre>\n<h4>Android</h4>\n<pre>git clone https://rastrahm@bitbucket.org/rastrahm/listtaskandroid.git</pre>\n<h4>React</h4>\n<pre>git clone https://rastrahm@bitbucket.org/rastrahm/listtaskreact.git</pre>\n<h4>Server PHP</h4>\n<pre>git clone https://rastrahm@bitbucket.org/rastrahm/listtaskserver.git</pre>\n	Descargas
4	<h1>Objetivos</h1>	<h1>Objetivos</h1>\n<h2>Descripción</h2>\n<p>El desarrollo corresponde al área web del proyecto ListTask, en el mismo podemos editar y manejar lo mismo que la aplicación, pero a través de Internet, teniendo la posibilidad a través de los servicios RestFul de acceder a los datos desde la aplicación de escritorio y la de Android.</p>\n<h2>Funcionamiento</h2>\n<p>Los pasos para el uso de la aplicación:</p>\n<ol>\n<li>Registrar su usuario con un correo valido.</li>\n<li>Acceder al correo y pulsar sobre el enlace de activación</li>\n<li>Ingresar a la aplicación a través del usuario y clave registrado</li>\n</ol>\n<h2>Código y Funcionamineto</h2>\n<p>Por favor leer Código Desarrollado</p>	Objetivos
2	<h1>Código Desarrollado</h1>	<h3>Construcción del Código</h3>\n<p>El front end de la aplicación esta realizado íntegramente en React con Node.js (v12.20.1) y se encuentra en: <pre>git clone https://rastrahm@bitbucket.org/rastrahm/listtaskreact.git</pre>, el mismo comprende react, react-dom, reactstrap, react-router, bootstrap y react-skylight.</p>\n<p>El back end esta conformado por php 7.2, funciona como un RestFul a través del index.php, accediendo a una base de datos PostgreSQL, el código se encuentra en <pre>git clone https://rastrahm@bitbucket.org/rastrahm/listtaskserver.git</pre>, además en el directorio de db, existe un dump de la base de datos. El back end esta creado sobre la base del PDO, por lo que es fácilmente convertible a otros formatos de bases de datos, mientras respete la estructura de funciones y schemas.</p>\n<p>La base de datos se estructuro en 2 schema: public y program:</p>\n<ul>\n<li>program: Corresponde al área del sistema unicamente: estructura de menús, acceso básico de usuarios, parámetros para su funcionamiento, su acceso es denegado por omisión.</li>\n<li>pubic: Contiene datos propios de la aplicación, fuera del marco básico de la aplicación en si, además contiene las funciones de acceso al contenido de program.</li>\n<ul>\n\n	Codigo
5	<h1>Acerca de</h1>	<h2>Acerca de</h2>\n<p>Mi nombre es <b>Rolando Strahm</b>, soy programador desde 1.993, como todos he evolucionado con la tecnologías, cambiantes, viendo modas, evoluciones y revoluciones en el mundo informático.</p>\n<p>Mi historia comprende idiomas como Pascal, C, C++, Visual Basic (del 1.0 hasta el 6.0), PHP (desde el 4.0 hasta el 7.3) con servidores Apache y NGINX, JavaScript (solo y con jQuery, AngularJS), Node.js (React, Express, Micro, Gulp), Java tanto para aplicaciones de escritorio como EE (Frameworks Spring, Ajax, Spark y servidores WildFly y Tomcat), Delphi y Lazarus. Trabajando desde maquinas Linux (Debian – Ubuntu), he pasado por muchas de las bases de datos como dBase, PostgreSQL, MySQL, MariaDB, Microsoft SQL y ORACLE, tanto planificando las estructuras como manejándolas para su uso a través de programas.</p>\n<h4>Mis datos de contacto:</h4>\n<ul>\n<li>Cel. / WhapApps / Signal +595 982 250 448</li>\n<li>E-Mail: rastrahm@gmail.com</li>\n<li>Skype: rastrahm@hotmail.com</li>\n<li>LinkedIn: https://www.linkedin.com/in/rolando-strahm/</li>\n</ul>	Acerca
\.


--
-- TOC entry 3186 (class 0 OID 83706)
-- Dependencies: 218
-- Data for Name: profile; Type: TABLE DATA; Schema: program; Owner: rasg
--

COPY program.profile (pro_id, pro_name) FROM stdin;
0	Administrador
1	Usuario
\.


--
-- TOC entry 3188 (class 0 OID 83711)
-- Dependencies: 220
-- Data for Name: user; Type: TABLE DATA; Schema: program; Owner: rasg
--

COPY program."user" (usr_id, usr_name, usr_active, pro_id) FROM stdin;
13	admin	1	0
41	rastrahm	1	1
\.


--
-- TOC entry 3190 (class 0 OID 83717)
-- Dependencies: 222
-- Data for Name: user_pass; Type: TABLE DATA; Schema: program; Owner: rasg
--

COPY program.user_pass (psw_id, usr_id, psw_date, psw_pass) FROM stdin;
27	41	2021-02-09 17:17:08.324375	\\xeb5c4dad18a1801c183caf7b82744b49
11	13	2021-02-06 09:47:43.291452	\\x4d8bec073f52bdea
\.


--
-- TOC entry 3194 (class 0 OID 83729)
-- Dependencies: 226
-- Data for Name: grouptask; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.grouptask (usr_id, json) FROM stdin;
13	{"group":[{"id":1,"name":"Grupo 1","task":[{"id":1,"name":"Tarea 1.1"}]}]}
\.


--
-- TOC entry 3196 (class 0 OID 83737)
-- Dependencies: 228
-- Data for Name: user_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_detail (usr_id, usr_email, email_enviado) FROM stdin;
13	rastrahm@gmail.com	f
41	rastrahm@hotmail.com	f
\.


--
-- TOC entry 3246 (class 0 OID 0)
-- Dependencies: 204
-- Name: dictionary_dic_id_seq; Type: SEQUENCE SET; Schema: program; Owner: postgres
--

SELECT pg_catalog.setval('program.dictionary_dic_id_seq', 1, false);


--
-- TOC entry 3247 (class 0 OID 0)
-- Dependencies: 206
-- Name: log_log_id_seq; Type: SEQUENCE SET; Schema: program; Owner: postgres
--

SELECT pg_catalog.setval('program.log_log_id_seq', 1331, true);


--
-- TOC entry 3248 (class 0 OID 0)
-- Dependencies: 208
-- Name: menu_men_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.menu_men_id_seq', 1, false);


--
-- TOC entry 3249 (class 0 OID 0)
-- Dependencies: 210
-- Name: menu_options_men_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.menu_options_men_id_seq', 3, true);


--
-- TOC entry 3250 (class 0 OID 0)
-- Dependencies: 211
-- Name: menu_options_mop_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.menu_options_mop_id_seq', 1, false);


--
-- TOC entry 3251 (class 0 OID 0)
-- Dependencies: 212
-- Name: menu_options_pro_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.menu_options_pro_id_seq', 4, true);


--
-- TOC entry 3252 (class 0 OID 0)
-- Dependencies: 213
-- Name: menu_pro_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.menu_pro_id_seq', 4, true);


--
-- TOC entry 3253 (class 0 OID 0)
-- Dependencies: 215
-- Name: parameter_par_id_seq; Type: SEQUENCE SET; Schema: program; Owner: postgres
--

SELECT pg_catalog.setval('program.parameter_par_id_seq', 5, true);


--
-- TOC entry 3254 (class 0 OID 0)
-- Dependencies: 217
-- Name: preview_pre_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.preview_pre_id_seq', 12, true);


--
-- TOC entry 3255 (class 0 OID 0)
-- Dependencies: 219
-- Name: profile_pro_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.profile_pro_id_seq', 1, false);


--
-- TOC entry 3256 (class 0 OID 0)
-- Dependencies: 221
-- Name: user_id_usr_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.user_id_usr_seq', 41, true);


--
-- TOC entry 3257 (class 0 OID 0)
-- Dependencies: 223
-- Name: user_pass_psw_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.user_pass_psw_id_seq', 27, true);


--
-- TOC entry 3258 (class 0 OID 0)
-- Dependencies: 224
-- Name: user_pass_usr_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.user_pass_usr_id_seq', 1, true);


--
-- TOC entry 3259 (class 0 OID 0)
-- Dependencies: 225
-- Name: user_pro_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.user_pro_id_seq', 1, false);


--
-- TOC entry 3260 (class 0 OID 0)
-- Dependencies: 227
-- Name: grouptask_iduser_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.grouptask_iduser_seq', 1, false);


--
-- TOC entry 3261 (class 0 OID 0)
-- Dependencies: 229
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hibernate_sequence', 27, true);


--
-- TOC entry 3017 (class 2606 OID 83756)
-- Name: dictionary id_dictionary_pk; Type: CONSTRAINT; Schema: program; Owner: postgres
--

ALTER TABLE ONLY program.dictionary
    ADD CONSTRAINT id_dictionary_pk PRIMARY KEY (dic_id);


--
-- TOC entry 3019 (class 2606 OID 83758)
-- Name: log ind_log; Type: CONSTRAINT; Schema: program; Owner: postgres
--

ALTER TABLE ONLY program.log
    ADD CONSTRAINT ind_log PRIMARY KEY (log_id);


--
-- TOC entry 3021 (class 2606 OID 83760)
-- Name: menu ind_menu; Type: CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu
    ADD CONSTRAINT ind_menu PRIMARY KEY (men_id);


--
-- TOC entry 3023 (class 2606 OID 83762)
-- Name: menu_options ind_menu_options; Type: CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu_options
    ADD CONSTRAINT ind_menu_options PRIMARY KEY (mop_id);


--
-- TOC entry 3025 (class 2606 OID 83764)
-- Name: parameter ind_parameter; Type: CONSTRAINT; Schema: program; Owner: postgres
--

ALTER TABLE ONLY program.parameter
    ADD CONSTRAINT ind_parameter PRIMARY KEY (par_id);


--
-- TOC entry 3027 (class 2606 OID 83766)
-- Name: preview ind_preview; Type: CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.preview
    ADD CONSTRAINT ind_preview PRIMARY KEY (pre_id);


--
-- TOC entry 3029 (class 2606 OID 83768)
-- Name: profile ind_profile; Type: CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.profile
    ADD CONSTRAINT ind_profile PRIMARY KEY (pro_id);


--
-- TOC entry 3033 (class 2606 OID 83770)
-- Name: user_pass ind_user_pass; Type: CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.user_pass
    ADD CONSTRAINT ind_user_pass PRIMARY KEY (psw_id);


--
-- TOC entry 3031 (class 2606 OID 83772)
-- Name: user ind_usuario; Type: CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program."user"
    ADD CONSTRAINT ind_usuario PRIMARY KEY (usr_id);


--
-- TOC entry 3035 (class 2606 OID 83774)
-- Name: user_detail user_detail_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_detail
    ADD CONSTRAINT user_detail_un UNIQUE (usr_email);


--
-- TOC entry 3169 (class 2618 OID 83775)
-- Name: user user_insert; Type: RULE; Schema: program; Owner: rasg
--

CREATE RULE user_insert AS
    ON INSERT TO program."user" DO  INSERT INTO program.log (log_date, log_time, log_event)
  VALUES (CURRENT_DATE, CURRENT_TIME, concat('User, insertar Usuario ', new.usr_name, ', perfil: ', new.pro_id));


--
-- TOC entry 3170 (class 2618 OID 83776)
-- Name: user user_update; Type: RULE; Schema: program; Owner: rasg
--

CREATE RULE user_update AS
    ON UPDATE TO program."user" DO  INSERT INTO program.log (log_date, log_time, log_event)
  VALUES (CURRENT_DATE, CURRENT_TIME, concat('User, actualización Usuario: ', new.usr_name, ', Activo: ', new.usr_active, ', Perfil: ', new.pro_id));


--
-- TOC entry 3037 (class 2606 OID 83777)
-- Name: menu_options ind_menu_options_menu; Type: FK CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu_options
    ADD CONSTRAINT ind_menu_options_menu FOREIGN KEY (men_id) REFERENCES program.menu(men_id);


--
-- TOC entry 3038 (class 2606 OID 83782)
-- Name: menu_options ind_menu_options_profile; Type: FK CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu_options
    ADD CONSTRAINT ind_menu_options_profile FOREIGN KEY (pro_id) REFERENCES program.profile(pro_id);


--
-- TOC entry 3036 (class 2606 OID 83787)
-- Name: menu ind_menu_profile; Type: FK CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu
    ADD CONSTRAINT ind_menu_profile FOREIGN KEY (pro_id) REFERENCES program.profile(pro_id);


--
-- TOC entry 3040 (class 2606 OID 83792)
-- Name: user_pass ind_user_pass-user; Type: FK CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.user_pass
    ADD CONSTRAINT "ind_user_pass-user" FOREIGN KEY (usr_id) REFERENCES program."user"(usr_id);


--
-- TOC entry 3039 (class 2606 OID 83797)
-- Name: user ind_user_profile; Type: FK CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program."user"
    ADD CONSTRAINT ind_user_profile FOREIGN KEY (pro_id) REFERENCES program.profile(pro_id);


--
-- TOC entry 3041 (class 2606 OID 83802)
-- Name: grouptask grouptask_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grouptask
    ADD CONSTRAINT grouptask_fk FOREIGN KEY (usr_id) REFERENCES program."user"(usr_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3042 (class 2606 OID 83807)
-- Name: user_detail user_detail_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_detail
    ADD CONSTRAINT user_detail_fk FOREIGN KEY (usr_id) REFERENCES program."user"(usr_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3225 (class 0 OID 0)
-- Dependencies: 215
-- Name: SEQUENCE parameter_par_id_seq; Type: ACL; Schema: program; Owner: postgres
--

GRANT ALL ON SEQUENCE program.parameter_par_id_seq TO rasg;


-- Completed on 2021-03-07 18:05:00 -03

--
-- PostgreSQL database dump complete
--

