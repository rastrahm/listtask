--
-- PostgreSQL database dump
--

-- Dumped from database version 10.15 (Ubuntu 10.15-1.pgdg18.04+1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-1.pgdg18.04+1)

-- Started on 2020-12-25 11:03:38 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3004 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

--
-- TOC entry 201 (class 1259 OID 290713)
-- Name: grouptask; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grouptask (
    iduser bigint NOT NULL,
    id bigint NOT NULL,
    name text
);


ALTER TABLE public.grouptask OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 290711)
-- Name: grouptask_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.grouptask_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grouptask_id_seq OWNER TO postgres;

--
-- TOC entry 3005 (class 0 OID 0)
-- Dependencies: 200
-- Name: grouptask_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.grouptask_id_seq OWNED BY public.grouptask.id;


--
-- TOC entry 199 (class 1259 OID 290709)
-- Name: grouptask_iduser_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.grouptask_iduser_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grouptask_iduser_seq OWNER TO postgres;

--
-- TOC entry 3006 (class 0 OID 0)
-- Dependencies: 199
-- Name: grouptask_iduser_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.grouptask_iduser_seq OWNED BY public.grouptask.iduser;


--
-- TOC entry 206 (class 1259 OID 290731)
-- Name: pass; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pass (
    iduser bigint NOT NULL,
    id bigint NOT NULL,
    pass character varying(50)
);


ALTER TABLE public.pass OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 290729)
-- Name: pass_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pass_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pass_id_seq OWNER TO postgres;

--
-- TOC entry 3007 (class 0 OID 0)
-- Dependencies: 205
-- Name: pass_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pass_id_seq OWNED BY public.pass.id;


--
-- TOC entry 204 (class 1259 OID 290727)
-- Name: pass_iduser_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pass_iduser_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pass_iduser_seq OWNER TO postgres;

--
-- TOC entry 3008 (class 0 OID 0)
-- Dependencies: 204
-- Name: pass_iduser_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pass_iduser_seq OWNED BY public.pass.iduser;


--
-- TOC entry 198 (class 1259 OID 290701)
-- Name: task; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.task (
    "group" bigint NOT NULL,
    id bigint NOT NULL,
    name text
);


ALTER TABLE public.task OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 290697)
-- Name: task_group_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.task_group_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.task_group_seq OWNER TO postgres;

--
-- TOC entry 3009 (class 0 OID 0)
-- Dependencies: 196
-- Name: task_group_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.task_group_seq OWNED BY public.task."group";


--
-- TOC entry 197 (class 1259 OID 290699)
-- Name: task_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.task_id_seq OWNER TO postgres;

--
-- TOC entry 3010 (class 0 OID 0)
-- Dependencies: 197
-- Name: task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.task_id_seq OWNED BY public.task.id;


--
-- TOC entry 203 (class 1259 OID 290723)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id bigint NOT NULL,
    "user" character varying(50)
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 290721)
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- TOC entry 3011 (class 0 OID 0)
-- Dependencies: 202
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- TOC entry 2853 (class 2604 OID 290717)
-- Name: grouptask id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grouptask ALTER COLUMN id SET DEFAULT nextval('public.grouptask_id_seq'::regclass);


--
-- TOC entry 2855 (class 2604 OID 290735)
-- Name: pass id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pass ALTER COLUMN id SET DEFAULT nextval('public.pass_id_seq'::regclass);


--
-- TOC entry 2852 (class 2604 OID 290705)
-- Name: task id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task ALTER COLUMN id SET DEFAULT nextval('public.task_id_seq'::regclass);


--
-- TOC entry 2854 (class 2604 OID 290726)
-- Name: user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- TOC entry 2993 (class 0 OID 290713)
-- Dependencies: 201
-- Data for Name: grouptask; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.grouptask VALUES (1, 1, 'Grupo 1');


--
-- TOC entry 2998 (class 0 OID 290731)
-- Dependencies: 206
-- Data for Name: pass; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.pass VALUES (1, 1, 'prueba');
INSERT INTO public.pass VALUES (1, 2, 'password');


--
-- TOC entry 2990 (class 0 OID 290701)
-- Dependencies: 198
-- Data for Name: task; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.task VALUES (1, 1, 'Tarea 1.1');
INSERT INTO public.task VALUES (1, 2, 'Tarea 1.2');


--
-- TOC entry 2995 (class 0 OID 290723)
-- Dependencies: 203
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."user" VALUES (1, 'rastrahm');


--
-- TOC entry 3012 (class 0 OID 0)
-- Dependencies: 200
-- Name: grouptask_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.grouptask_id_seq', 1, false);


--
-- TOC entry 3013 (class 0 OID 0)
-- Dependencies: 199
-- Name: grouptask_iduser_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.grouptask_iduser_seq', 1, false);


--
-- TOC entry 3014 (class 0 OID 0)
-- Dependencies: 205
-- Name: pass_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pass_id_seq', 1, false);


--
-- TOC entry 3015 (class 0 OID 0)
-- Dependencies: 204
-- Name: pass_iduser_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pass_iduser_seq', 1, false);


--
-- TOC entry 3016 (class 0 OID 0)
-- Dependencies: 196
-- Name: task_group_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.task_group_seq', 1, false);


--
-- TOC entry 3017 (class 0 OID 0)
-- Dependencies: 197
-- Name: task_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.task_id_seq', 1, false);


--
-- TOC entry 3018 (class 0 OID 0)
-- Dependencies: 202
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 1, false);


--
-- TOC entry 2859 (class 2606 OID 290748)
-- Name: grouptask grouptask_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grouptask
    ADD CONSTRAINT grouptask_pk PRIMARY KEY (id);


--
-- TOC entry 2863 (class 2606 OID 290737)
-- Name: pass pass_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pass
    ADD CONSTRAINT pass_pk PRIMARY KEY (id);


--
-- TOC entry 2857 (class 2606 OID 290746)
-- Name: task task_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_pk PRIMARY KEY (id);


--
-- TOC entry 2861 (class 2606 OID 290739)
-- Name: user user_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pk PRIMARY KEY (id);


--
-- TOC entry 2865 (class 2606 OID 290754)
-- Name: grouptask grouptask_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grouptask
    ADD CONSTRAINT grouptask_fk FOREIGN KEY (id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2866 (class 2606 OID 290740)
-- Name: pass pass_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pass
    ADD CONSTRAINT pass_fk FOREIGN KEY (iduser) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2864 (class 2606 OID 290759)
-- Name: task task_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_fk FOREIGN KEY ("group") REFERENCES public.grouptask(id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2020-12-25 11:03:38 -03

--
-- PostgreSQL database dump complete
--

