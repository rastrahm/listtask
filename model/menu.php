<?php
class menu {
    /**
    * Traemos solamente la parte superior del menu correspondiente al profile
    * param (profile) => Identifica al perfil del usuario debuelto en security->Select
    **/
    function getMenu($profile) {
        $query = "SELECT * FROM public.fnc_men(".$profile.")";
        return $query;
    }

    /**
    * Traemos los submenus
    * param (profile) => Identifica al perfil del usuario
    * param (menu) => Id del menu a buscar sus submenus
    **/
    function getSubmenu($profile, $menu) {
        $query = "SELECT * FROM public.fnc_men_sub(".$profile.", ".$menu.")";
        return $query;
    }
}

 ?>
