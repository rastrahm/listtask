<?php
class objects {
    //Construtor de query, tomar en cuenta como manejar el string con los campos correspondientes
    function QueryContruct($method, $function, $fields, $paranms, $name) {
        if (($method == "GET") AND ($name == $paranms['menu'])) {
            $str_function = "SELECT * FROM public.".$function.'_'.$method."_ALL(";
            $name = '';
        } else {
            $str_function = "SELECT * FROM public.".$function.'_'.$method."(";
        }
        $i = 0;
        if (($name) && (($paranms == null) OR (count($paranms) == 1))) {
            if (is_numeric($name)) {
                $str_function .= $name;
                $i = $i + 1;
            } else {
                $str_function .= "'".$name."'";
                $i = $i + 1;
            }
        }
        if ($method == "PUT") {
            foreach($paranms as $key1 => $value1) {
                if ($key1 == 'big_id') {
                    $str_function .= $value1;
                    $i = $i + 1;
                }
            }
        } elseif ($method == "DELETE") {
            $str_function .= "'".$paranms['delete']."'";
        }
        if ($paranms) {
            $arr_paranms = explode(",", str_replace("'", "", str_replace("{", "", str_replace("}", "", $fields))));
            $str_separator = "";
            foreach($arr_paranms as $key0 => $value0) {
                if ($i > 0) {
                    $str_separator = ",";
                }
                foreach($paranms as $key1 => $value1) {
                    if ($value0 == $key1) {
						if ((substr($value0, 0, 3) == 'str') || (substr($value0, 0, 3) == 'txt') || (substr($value0, 0, 3) == 'dat')){
							if (gettype($value1) == 'array') {
								$value1 = json_encode($value1);
							}
							$str_function .= $str_separator."'". str_replace('&gt;', '>', str_replace('&lt;', '<', $value1))."'";
						} else {
							$str_function .= $str_separator.$value1;
							switch(substr($value0, 0, 3)) {
								case 'big':
									$str_function .= '::bigint';
									break;
								case 'int':
									$str_function .= '::integer';
									break;
								case 'sml':
									$str_function .= '::smallint';
									break;
								case 'bol':
									$str_function .= '::smallint';
									break;
								case 'dec':
									$str_function .= '::decimal';
									break;
								case 'num':
									$str_function .= '::bigint';
									break;
								case 'rea':
									$str_function .= '::real';
									break;
								case 'dou':
									$str_function .= '::double';
									break;
								case 'sms':
									$str_function .= '::smallserial';
									break;
								case 'ser':
									$str_function .= '::serial';
									break;
								case 'bis':
									$str_function .= '::bigserial';
									break;
							}
						}
                    }
                }
                $i = $i + 1;
            }
        }
        $str_function .= ")";
        return $str_function;
	}
	
	function ReturnJson($stdId) {
		$query = "SELECT * FROM fnc_listtask_get(" . $stdId . ")";
        return $query;
	}

	function RequestJson($stdId, $stdValue) {
		$query = "SELECT * FROM fnc_listtask_post(" . $stdId . ", '" . $stdValue . "')";
        return $query;
	}
}
?>
